#include <graphics_framework.h>
#include <glm/glm.hpp>

using namespace std;
using namespace graphics_framework;
using namespace glm;

mesh sphere;
mesh skybox;
effect eff;
effect sky_eff;
cubemap cube_map;
//target_camera cam;
free_camera cam;

// Initial mouse input
double cursor_x = 0.0;
double cursor_y = 0.0;

bool initialise()
{
	// ********************************
	// Set input mode - hide the cursor
	// ********************************
	glfwSetInputMode(renderer::get_window(), GLFW_CURSOR, GLFW_CURSOR_DISABLED);

	// ******************************
	// Capture initial mouse position
	// ******************************
	glfwGetCursorPos(renderer::get_window(), &cursor_x, &cursor_y);

	return true;
}

bool load_content()
{
    // Create a sphere
    sphere = mesh(geometry_builder::create_sphere(25, 25));
    // ******************************
    // Create box geometry for skybox
    // ******************************
    geometry geom;
    geom.set_type(GL_QUADS);
    vector<vec3> positions
    {
		//// Front
		//vec3(-1.0f, 1.0f, 1.0f),		// 1
		//vec3(-1.0f, -1.0f, 1.0f),		// 3
		//vec3(1.0f, -1.0f, 1.0f),		// 4
		//vec3(1.0f, 1.0f, 1.0f),		// 2

		//// Right
		//vec3(1.0f, 1.0f, 1.0f),		// 2
		//vec3(1.0f, -1.0f, 1.0f),		// 4
		//vec3(1.0f, -1.0f, -1.0f),		// 8
		//vec3(1.0f, 1.0f, -1.0f),		// 6

		//// Back
		//vec3(1.0f, 1.0f, -1.0f),		// 6
		//vec3(1.0f, -1.0f, -1.0f),		// 8
		//vec3(-1.0f, -1.0f, -1.0f),	// 7
		//vec3(-1.0f, 1.0f, -1.0f),		// 5

		//// Left
		//vec3(-1.0f, 1.0f, -1.0f),		// 5
		//vec3(-1.0f, -1.0f, -1.0f),	// 7
		//vec3(-1.0f, -1.0f, 1.0f),		// 3
		//vec3(-1.0f, 1.0f, 1.0f),		// 1

		//// Top
		//vec3(-1.0f, 1.0f, -1.0f),		// 5
		//vec3(-1.0f, 1.0f, 1.0f),		// 1
		//vec3(1.0f, 1.0f, 1.0f),		// 2
		//vec3(1.0f, 1.0f, -1.0f),		// 6

		//// Bottom 
		//vec3(-1.0f, -1.0f, 1.0f),		// 3
		//vec3(-1.0f, -1.0f, -1.0f),	// 7
		//vec3(1.0f, -1.0f, -1.0f),		// 8
		//vec3(1.0f, -1.0f, 1.0f),		// 4

		vec3(-1.0f, 1.0f, -1.0f),	/* 5 */
		vec3(-1.0f, 1.0f, 1.0f),	/* 1 */
		vec3(-1.0f, -1.0f, 1.0f),	/* 2 */
		vec3(-1.0f, -1.0f, -1.0f),	/* 7 */

		vec3(1.0f, 1.0f, 1.0f),		/* 4 */
		vec3(1.0f, 1.0f, -1.0f),	/* 6 */
		vec3(1.0f, -1.0f, -1.0f),	/* 8 */
		vec3(1.0f, -1.0f, 1.0f),	/* 3 */

		vec3(-1.0f, 1.0f, -1.0f),	/* 5 */
		vec3(1.0f, 1.0f, -1.0f),	/* 6 */
		vec3(1.0f, 1.0f, 1.0f),		/* 4 */
		vec3(-1.0f, 1.0f, 1.0f),	/* 1 */

		vec3(-1.0f, -1.0f, 1.0f),	/* 2 */
		vec3(1.0f, -1.0f, 1.0f),	/* 3 */
		vec3(1.0f, -1.0f, -1.0f),	/* 8 */
		vec3(-1.0f, -1.0f, -1.0f),	/* 7 */

		vec3(-1.0f, -1.0f, -1.0f),	/* 7 */
		vec3(1.0f, -1.0f, -1.0f),	/* 8 */
		vec3(1.0f, 1.0f, -1.0f),	/* 6 */
		vec3(-1.0f, 1.0f, -1.0f),	/* 5 */

		vec3(1.0f, -1.0f, 1.0f),	/* 3 */
		vec3(-1.0f, -1.0f, 1.0f),	/* 2 */
		vec3(-1.0f, 1.0f, 1.0f),	/* 1 */
		vec3(1.0f, 1.0f, 1.0f)		/* 4 */

		//// Right
		//vec3(1.0f, 1.0f, 0.0f),
		//vec3(1.0f, 1.0f, -2.0f),
		//vec3(1.0f, -1.0f, -2.0f),
		//vec3(1.0f, -1.0f, 0.0f),

		//// Left
		//vec3(-1.0f, 1.0f, 0.0f),
		//vec3(-1.0f, -1.0f, 0.0f),
		//vec3(-1.0f, -1.0f, -2.0f),
		//vec3(-1.0f, 1.0f, -2.0f),

		//// Top
		//vec3(-1.0f, 1.0f, 0.0f),
		//vec3(-1.0f, 1.0f, -2.0f),
		//vec3(1.0f, 1.0f, -2.0f),
		//vec3(1.0f, 1.0f, 0.0f),

		//// Back 
		//vec3(-1.0f, 1.0f, -2.0f),
		//vec3(-1.0f, -1.0f, -2.0f),
		//vec3(1.0f, -1.0f, -2.0f),
		//vec3(1.0f, 1.0f, -2.0f),

		//// Front
		//vec3(1.0f, 1.0f, 0.0f),
		//vec3(1.0f, -1.0f, 0.0f),
		//vec3(-1.0f, -1.0f, 0.0f),
		//vec3(-1.0f, 1.0f, 0.0f),

		//// Bottom
		//vec3(1.0f, -1.0f, 0.0f),
		//vec3(1.0f, -1.0f, -2.0f),
		//vec3(-1.0f, -1.0f, -2.0f),
		//vec3(-1.0f, -1.0f, 0.0f)
	
        
    };
    geom.add_buffer(positions, BUFFER_INDEXES::POSITION_BUFFER);
    skybox = mesh(geom);
    // ***********************************
    // Scale box by 100 - allows a distance
    // ***********************************
	skybox.get_transform().scale *= 100;

    // ******************************************************
    // Load the cubemap
    // - create array of six filenames +x, -x, +y, -y, +z, -z
    // ******************************************************
    array<string, 6> filenames =
    {
		
		"..\\resources\\textures\\cubemaps\\DeepSpaceGreenWithPlanet\\rightImage.png",
		"..\\resources\\textures\\cubemaps\\DeepSpaceGreenWithPlanet\\leftImage.png",
		"..\\resources\\textures\\cubemaps\\DeepSpaceGreenWithPlanet\\upImage.png",
		"..\\resources\\textures\\cubemaps\\DeepSpaceGreenWithPlanet\\downImage.png",
		"..\\resources\\textures\\cubemaps\\DeepSpaceGreenWithPlanet\\frontImage.png",
		"..\\resources\\textures\\cubemaps\\DeepSpaceGreenWithPlanet\\backImage.png"
		//"..\\resources\\textures\\cubemaps\\alien\\posx.png",
		//"..\\resources\\textures\\cubemaps\\alien\\negx.png",
		//"..\\resources\\textures\\cubemaps\\alien\\posy.png",
		//"..\\resources\\textures\\cubemaps\\alien\\negy.png",
		//"..\\resources\\textures\\cubemaps\\alien\\posz.png",
		//"..\\resources\\textures\\cubemaps\\alien\\negz.png"
    };
    // ***************
    // Create cube_map
    // ***************
	cube_map = cubemap(filenames);

    //// Load in shaders
    //eff.add_shader("shader.vert", GL_VERTEX_SHADER);
    //eff.add_shader("shader.frag", GL_FRAGMENT_SHADER);
    //// Build effect
    //eff.build();

    // *********************
    // Load in skybox effect
    // *********************
	sky_eff.add_shader("../resources/shaders/skybox.vert", GL_VERTEX_SHADER);
	sky_eff.add_shader("../resources/shaders/skybox.frag", GL_FRAGMENT_SHADER);
    // Build effect
    sky_eff.build();

    // Set camera properties
    cam.set_position(vec3(0.0f, 0.0f, 10.0f));
    cam.set_target(vec3(0.0f, 0.0f, 0.0f));
    auto aspect = static_cast<float>(renderer::get_screen_width()) / static_cast<float>(renderer::get_screen_height());
    cam.set_projection(quarter_pi<float>(), aspect, 2.414f, 1000.0f);
    return true;
}

bool update(float delta_time)
{
	// The ratio of pixels to rotation - remember the fov
	static double ratio_width = quarter_pi<float>() / static_cast<float>(renderer::get_screen_width());
	static double ratio_height = (quarter_pi<float>() * (static_cast<float>(renderer::get_screen_height()) / static_cast<float>(renderer::get_screen_width()))) / static_cast<float>(renderer::get_screen_height());

	double current_x;
	double current_y;
	// *******************************
	// Get the current cursor position
	// *******************************
	glfwGetCursorPos(renderer::get_window(), &current_x, &current_y);


	// ***************************************************
	// Calculate delta of cursor positions from last frame
	// ***************************************************
	double delta_x = current_x - cursor_x;
	double delta_y = current_y - cursor_y;

	// *************************************************************
	// Multiply deltas by ratios - gets actual change in orientation
	// *************************************************************
	delta_x = delta_x * ratio_width;
	delta_y = delta_y * ratio_height;

	// *************************
	// Rotate cameras by delta
	// delta_y - x-axis rotation
	// delta_x - y-axis rotation
	// *************************
	cam.rotate(delta_x, -delta_y); // -delta_y, otherwise up and down is reversed

	// *******************************
	// Use keyboard to move the camera
	// - WSAD
	// *******************************

	const float speed = 1.0f; // Speed of camera movement

	vec3 translation(0.0f, 0.0f, 0.0f);

	// Check if key is pressed
	if (glfwGetKey(renderer::get_window(), GLFW_KEY_W))
		cam.move(vec3(0.0f, 0.0f, speed)); // translation = vec3(0.0f, 0.0f, 1.0f); (If you do it this way then you cant have 2 translations at the same time)
	if (glfwGetKey(renderer::get_window(), GLFW_KEY_S))
		cam.move(vec3(0.0f, 0.0f, -speed)); // translation = vec3(0.0f, 0.0f, -1.0f);
	if (glfwGetKey(renderer::get_window(), GLFW_KEY_A))
		cam.move(vec3(-speed, 0.0f, 0.0f)); // translation = vec3(-1.0f, 0.0f, 0.0f);
	if (glfwGetKey(renderer::get_window(), GLFW_KEY_D))
		cam.move(vec3(speed, 0.0f, 0.0f)); // translation = vec3(1.0f, 0.0f, 0.0f);


	// *****************
	// Update the camera
	// *****************
	cam.update(delta_time);

	// *******************************************************************
	// Set skybox position to camera position (camera in centre of skybox)
	// *******************************************************************
	skybox.get_transform().position = cam.get_position();

	// *****************
	// Update cursor pos
	// *****************
	cursor_x = current_x;
	cursor_y = current_y;


    return true;
}

bool render()
{
    // *********************************
    // Disable depth test and depth mask
    // *********************************
	glDisable(GL_DEPTH_TEST);
	glDepthMask(GL_FALSE);
	//glDisable(GL_CULL_FACE);

    // ******************
    // Bind skybox effect
    // ******************
	renderer::bind(sky_eff);

    // ****************************
    // Calculate MVP for the skybox
    // ****************************
	auto M = skybox.get_transform().get_transform_matrix();
	auto V = cam.get_view();								
	auto P = cam.get_projection();							
	auto MVP = P * V * M;									

    // **********************
    // Set MVP matrix uniform
    // **********************
	glUniformMatrix4fv(
		sky_eff.get_uniform_location("MVP"),
		1,
		GL_FALSE,
		value_ptr(MVP));

    // *******************
    // Set cubemap uniform
    // *******************
	renderer::bind(cube_map, 0);

    // *************
    // Render skybox
    // *************
	renderer::render(skybox);

    // ********************************
    // Enable depth test and depth mask
    // ********************************
	glDepthMask(GL_TRUE);
	glEnable(GL_DEPTH_TEST);

    //// Bind effect
    //renderer::bind(eff);
    //// Create MVP matrix
    //M = sphere.get_transform().get_transform_matrix();
    //V = cam.get_view();
    //P = cam.get_projection();
    //MVP = P * V * M;
    //// Set MVP matrix uniform
    //glUniformMatrix4fv(
    //    eff.get_uniform_location("MVP"),
    //    1,
    //    GL_FALSE,
    //    value_ptr(MVP));
    //// Render mesh
    //renderer::render(sphere);

    return true;
}

void main()
{
    // Create application
    app application;
    // Set methods
    application.set_load_content(load_content);
	application.set_initialise(initialise); // free camera stuff
    application.set_update(update);
    application.set_render(render);
    // Run application
    application.run();
}