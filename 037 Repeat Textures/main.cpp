#include <graphics_framework.h>
#include <glm\glm.hpp>
#include <memory>

using namespace std;
using namespace graphics_framework;
using namespace glm;

mesh m;
effect eff;
target_camera cam;
texture tex;

vec3 pos = m.get_transform().position;

float theta = 20.0f;
float rho = 0.0f;

vec3 xrot(1.0f, 0.0f, 0.0f);
vec3 yrot(0.0f, 1.0f, 0.0f);

bool load_content()
{
	// Construct geometry object
	geometry geom;
	geom.set_type(GL_QUADS);
	// Create triangle data
	// Positions
	vector<vec3> positions
	{
		vec3(1.0f, 1.0f, 0.0f),
		vec3(-1.0f, 1.0f, 0.0f),
		vec3(-1.0f, -1.0f, 0.0f),
		vec3(1.0f, -1.0f, 0.0f)
	};
	// *****************************************************
	// Define texture coordinates for quad - remember repeat
	// *****************************************************
	vector<vec2> tex_coord
	{
		vec2(3.0f, 3.0f),
		vec2(-1.0f, 3.0f),
		vec2(-1.0f, -1.0f),
		vec2(3.0f, -1.0f)
	};

	// Add positions to the geometry
	geom.add_buffer(positions, BUFFER_INDEXES::POSITION_BUFFER);

	// *****************************************
	// Add texture coordinate buffer to geometry
	// *****************************************
	geom.add_buffer(tex_coord, BUFFER_INDEXES::TEXTURE_COORDS_0);

	// Create mesh object
	m = mesh(geom);

	// ****************************
	// Load in texture shaders here
	// ****************************

	eff.add_shader("../resources/shaders/simple_texture.vert", GL_VERTEX_SHADER);
	eff.add_shader("../resources/shaders/simple_texture.frag", GL_FRAGMENT_SHADER);


	// ************
	// Build effect
	// ************
	eff.build();

	// *********************
	// Load texture sign.jpg
	// *********************
	tex = texture("C:/Users/Manos/Downloads/akis.jpg");

	// Set camera properties
	cam.set_position(vec3(0.0f, 0.0f, 5.0f));
	cam.set_target(vec3(0.0f, 0.0f, 0.0f));
	auto aspect = static_cast<float>(renderer::get_screen_width()) / static_cast<float>(renderer::get_screen_height());
	cam.set_projection(quarter_pi<float>(), aspect, 2.414f, 1000.0f);

	return true;
}

bool update(float delta_time)
{

	// WSAD - movement
	const float speed = 5.0f;

	// Check if key is pressed
	if (glfwGetKey(renderer::get_window(), GLFW_KEY_W))
		pos += vec3(0.0f, 0.0f, -speed) * delta_time;
	if (glfwGetKey(renderer::get_window(), GLFW_KEY_S))
		pos += vec3(0.0f, 0.0f, speed) * delta_time;
	if (glfwGetKey(renderer::get_window(), GLFW_KEY_A))
		pos += vec3(-speed, 0.0f, 0.0f) * delta_time;
	if (glfwGetKey(renderer::get_window(), GLFW_KEY_D))
		pos += vec3(speed, 0.0f, 0.0f) * delta_time;
	if (glfwGetKey(renderer::get_window(), GLFW_KEY_E))
		pos += vec3(0.0f, speed, 0.0f) * delta_time;
	if (glfwGetKey(renderer::get_window(), GLFW_KEY_Q))
		pos += vec3(0.0f, -speed, 0.0f) * delta_time;

	if (glfwGetKey(renderer::get_window(), GLFW_KEY_LEFT))
		theta -= pi<float>() * delta_time;
	if (glfwGetKey(renderer::get_window(), GLFW_KEY_RIGHT))
		theta += pi<float>() * delta_time;
	if (glfwGetKey(renderer::get_window(), GLFW_KEY_UP))
		rho -= pi<float>() * delta_time;
	if (glfwGetKey(renderer::get_window(), GLFW_KEY_DOWN))
		rho += pi<float>() * delta_time;

	// Update the camera
	cam.update(delta_time);
	return true;
}

bool render()
{
	// Bind effect
	renderer::bind(eff);
	// Create MVP matrix

	mat4 M = m.get_transform().get_transform_matrix();;
	mat4 T = translate(M, pos);

	quat rx = rotate(m.get_transform().orientation, rho, xrot);
	quat ry = rotate(m.get_transform().orientation, theta, yrot);

	mat4 R = mat4_cast(rx * ry);
	M = T * R;

	//auto M = m.get_transform().get_transform_matrix();
	auto V = cam.get_view();
	auto P = cam.get_projection();
	auto MVP = P * V * M;
	// Set MVP matrix uniform
	glUniformMatrix4fv(
		eff.get_uniform_location("MVP"), // Location of uniform
		1, // Number of values - 1 mat4
		GL_FALSE, // Transpose the matrix?
		value_ptr(MVP)); // Pointer to matrix data

	// ************************
	// Bind texture to renderer
	// ************************
	renderer::bind(tex, 0);

	// *****************************************
	// Set the texture value for the shader here
	// *****************************************
	glUniform1i(eff.get_uniform_location("tex"), 0);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_MIRRORED_REPEAT); // width?
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_MIRRORED_REPEAT); // height?

	// Render the mesh
	renderer::render(m);

	return true;
}

void main()
{
	// Create application
	app application;
	// Set load content, update and render methods
	application.set_load_content(load_content);
	application.set_update(update);
	application.set_render(render);
	// Run application
	application.run();
}