#include <graphics_framework.h>
#include <glm\glm.hpp>
#include <GLM\gtc\quaternion.hpp>
using namespace std;
using namespace graphics_framework;
using namespace glm;

mesh m;
effect eff;
target_camera cam;

vec3 pos = m.get_transform().position;
vec3 sc = m.get_transform().scale;
quat orient = m.get_transform().orientation;

double xpos, ypos, xposNext, yposNext;

float theta = 20.0f;
float rho = 0.0f;

vec3 xrot(1.0f, 0.0f, 0.0f);
vec3 yrot(0.0f, 1.0f, 0.0f);

quat rx, ry;

bool load_content()
{
	// Construct geometry object
	geometry geom;
	// Create triangle data
	// Positions
	vector<vec3> positions
	{
		// Front
		vec3(0.0f, 1.0f, 0.0f),
		vec3(-1.0f, -1.0f, 0.0f),
		vec3(1.0f, -1.0f, 0.0f),
		// Back
		vec3(1.0f, -1.0f, 0.0f),
		vec3(-1.0f, -1.0f, 0.0f),
		vec3(0.0f, 1.0f, 0.0f)
	};
	// Colours
	vector<vec4> colours
	{
		vec4(1.0f, 0.0f, 0.0f, 1.0f),
		vec4(1.0f, 0.0f, 0.0f, 1.0f),
		vec4(1.0f, 0.0f, 0.0f, 1.0f),

		vec4(1.0f, 0.0f, 0.0f, 1.0f),
		vec4(1.0f, 0.0f, 0.0f, 1.0f),
		vec4(1.0f, 0.0f, 0.0f, 1.0f)
	};
	// Add to the geometry
	geom.add_buffer(positions, BUFFER_INDEXES::POSITION_BUFFER);
	geom.add_buffer(colours, BUFFER_INDEXES::COLOUR_BUFFER);

	// ***********************
	// Create mesh object here
	// ***********************

	m = mesh(geom);


	// Load in shaders
	eff.add_shader(
		"..\\resources\\shaders\\basic.vert", // filename
		GL_VERTEX_SHADER); // type
	eff.add_shader(
		"..\\resources\\shaders\\basic.frag", // filename
		GL_FRAGMENT_SHADER); // type
	// Build effect
	eff.build();

	// Set camera properties
	cam.set_position(vec3(10.0f, 10.0f, 10.0f));
	cam.set_target(vec3(0.0f, 0.0f, 0.0f));
	auto aspect = static_cast<float>(renderer::get_screen_width()) / static_cast<float>(renderer::get_screen_height());
	cam.set_projection(quarter_pi<float>(), aspect, 2.414f, 1000.0f);
	return true;
}

bool update(float delta_time)
{

	// Increment theta - half a rotation per second
	//theta += pi<float>() * delta_time;

	// Use keys to update transform values

	// WSAD - movement
	const float speed = 5.0f;

	// Check if key is pressed
	if (glfwGetKey(renderer::get_window(), GLFW_KEY_W))
		pos += vec3(0.0f, 0.0f, -speed) * delta_time;
	if (glfwGetKey(renderer::get_window(), GLFW_KEY_S))
		pos += vec3(0.0f, 0.0f, speed) * delta_time;
	if (glfwGetKey(renderer::get_window(), GLFW_KEY_A))
		pos += vec3(-speed, 0.0f, 0.0f) * delta_time;
	if (glfwGetKey(renderer::get_window(), GLFW_KEY_D))
		pos += vec3(speed, 0.0f, 0.0f) * delta_time;
	if (glfwGetKey(renderer::get_window(), GLFW_KEY_E))
		pos += vec3(0.0f, speed, 0.0f) * delta_time;
	if (glfwGetKey(renderer::get_window(), GLFW_KEY_Q))
		pos += vec3(0.0f, -speed, 0.0f) * delta_time;

	// Cursor - rotation

	int state = glfwGetMouseButton(renderer::get_window(), GLFW_MOUSE_BUTTON_LEFT);
	
	if (state == GLFW_PRESS)
	{
		// Makes cursor invisible
		glfwSetInputMode(renderer::get_window(), GLFW_CURSOR, GLFW_CURSOR_DISABLED);

		// Store x and y positions before updating them 
		// to see in which direction the object has to rotate.
		xpos = xposNext;
		ypos = yposNext;

		// Gets cursor position
		glfwGetCursorPos(renderer::get_window(), &xposNext, &yposNext);

		// Makes centre of screen origin points x,y(0.0,0.0)
		xposNext -= renderer::get_screen_width() / 2.0f;
		yposNext -= renderer::get_screen_height() / 2.0f;

		// Rotate right
		if (yposNext > ypos)
			rho -= pi<float>()*2 * delta_time;
		// Rotate left
		if (yposNext < ypos)
			rho += pi<float>()*2 * delta_time;
		// Rotate down
		if (xposNext > xpos)
			theta += pi<float>()*2 * delta_time;
		// Rotate up
		if (xposNext < xpos)
			theta -= pi<float>()*2 * delta_time;

		cout << "x = " << xpos << endl;
		cout << "y = " << ypos << endl;
		
		

		if (glfwGetKey(renderer::get_window(), GLFW_KEY_UP))
			theta -= pi<float>() * delta_time;
		if (glfwGetKey(renderer::get_window(), GLFW_KEY_DOWN))
			theta += pi<float>() * delta_time;
		if (glfwGetKey(renderer::get_window(), GLFW_KEY_RIGHT))
			rho -= pi<float>() * delta_time;
		if (glfwGetKey(renderer::get_window(), GLFW_KEY_LEFT))
			rho += pi<float>() * delta_time;
	}
	else
	{
		// Makes cursor visible again
		glfwSetInputMode(renderer::get_window(), GLFW_CURSOR, GLFW_CURSOR_NORMAL);
	}

	// O decrease scale, P increase scale

	if (glfwGetKey(renderer::get_window(), GLFW_KEY_P))
		sc += 0.1f;

	if (glfwGetKey(renderer::get_window(), GLFW_KEY_O))
		sc -= 0.1f;


	// Update the camera
	cam.update(delta_time);
	return true;
}

bool render()
{
	// Bind effect
	renderer::bind(eff);
	// *************************************
	// Get the model transform from the mesh
	//**************************************
	mat4 M = m.get_transform().get_transform_matrix();;
	mat4 T = translate(M, pos);
	mat4 S = scale(M, sc);

	rx = rotate(quat(), rho, xrot);
	ry = rotate(quat(), theta, yrot);

	mat4 R = mat4_cast(rx * ry); // *ry;

	M = T * (R * S);

	// Create MVP matrix
	auto V = cam.get_view();
	auto P = cam.get_projection();
	auto MVP = P * V * M;
	// Set MVP matrix uniform
	glUniformMatrix4fv(
		eff.get_uniform_location("MVP"), // Location of uniform
		1, // Number of values - 1 mat4
		GL_FALSE, // Transpose the matrix?
		value_ptr(MVP)); // Pointer to matrix data

	// ********************
	// Render the mesh here
	// ********************

	renderer::render(m);
	

	return true;
}

void main()
{
	// Create application
	app application;
	// Set load content, update and render methods
	application.set_load_content(load_content);
	application.set_update(update);
	application.set_render(render);
	// Run application
	application.run();
}