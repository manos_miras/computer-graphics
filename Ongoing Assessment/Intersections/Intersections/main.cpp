#include "Intersections.h"
#include <iostream>
#include <Windows.h>
using namespace std;

void main()
{
	int waitforinput;
	while (true)
	{ 
		Vector3 point(3.0f, 8.0f, -1.0f);
		Vector3 normal(0.5f, 1.0f, 0.0f);
		//Vector3 normal(4.0f, -1.0f, 2.0f);
		//Vector3 pointOnPlane(7.0f, 11.0f, 3.0f);
		Vector3 pointOnPlane(rand() % 10, rand() % 10, rand() % 10);
		//pointOnPlane = pointOnPlane / 42.0f;
		cout << "Point on plane: " << endl;
		cout << pointOnPlane.x << " " << pointOnPlane.y << " " << pointOnPlane.z << endl;

		Vector3 res = ClosestPointToPlane(point, pointOnPlane, normal);

		//cout << "Closest point: " << endl;
		//cout << res.x << " " << res.y << " " << res.z << endl;

		//cout << IsPointInFrontOfPlane(point, pointOnPlane, normal) << endl;
		
		//RAYS:
		//RAYS and PLANES
		Vector3 rayOrigin(3.0f, 10.0f, -2.0f);
		Vector3 rayDirection(0.0f, -1.0f, 0.0f);
		Vector3 outInterPoint;

		cout << RayPlaneIntersection(rayOrigin, rayDirection, pointOnPlane, normal, outInterPoint) << endl;

		//RAYS and SPHERES

		//RAYS and TRIANGLES

		//cout << normal.x << " " << normal.y << " " << normal.z << endl;

		//if (IsPointInFrontOfPlane(point, pointOnPlane, normal))
		//	cout << "point is in front of plane." << endl;
		//else
		//	cout << "point is NOT in front of plane." << endl;

		cin.get();
		//cin >> waitforinput;
	}
}