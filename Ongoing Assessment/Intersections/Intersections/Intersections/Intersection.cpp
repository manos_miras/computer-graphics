#include "Intersection.h"
#include <math.h>
#include <iostream>
using namespace std;

Vector3 ClosestPointToPlane(const Vector3& point,
	const Vector3& pointOnPlane, const Vector3& planeNormal)
{
	Vector3 closestPoint = point +  planeNormal * (Vector3::Dot(pointOnPlane, planeNormal) - Vector3::Dot(point, planeNormal));
	return closestPoint;
}

bool IsPointInFrontOfPlane(const Vector3& point,
	const Vector3& pointOnPlane, const Vector3& planeNormal)
{
	Vector3 distance = point - pointOnPlane;

	float res = Vector3::Dot(distance, planeNormal);
	
	if (res > 0)
		return true; // Point is in front of plane
	else
		return false; // Point is not in front of plane
}

bool RayPlaneIntersection(const Vector3& rayOrigin, const Vector3& rayDirection,
	const Vector3& pointOnPlane, const Vector3& planeNormal,
	Vector3& outIntersectionPoint)
{
	float t = 0;
	if (Vector3::Dot(rayDirection, planeNormal) != 0)
	{
		t = (Vector3::Dot(pointOnPlane, planeNormal) - Vector3::Dot(rayOrigin, planeNormal)) /
			 Vector3::Dot(rayDirection, planeNormal);
		
		cout <<"t: " << t << endl;
	}
	else
	{
		//Ray is parallel to the plane (t = 0)
		return false;
	}
	
	if (t > 0)
	{
		outIntersectionPoint = rayOrigin + rayDirection * t;
		cout << "inter point: " << outIntersectionPoint.x << " " <<
			outIntersectionPoint.y << " " <<
			outIntersectionPoint.z << endl;
		return true;
	}
	// There is no intersection
	else
		return false;
}

bool RaySphereIntersect(const Vector3& sphereCenter, const float sphereRadius,
	const Vector3& rayOrigin, const Vector3& rayDirection,
	Vector3& outIntersectionPoint)
{
	Vector3 e = sphereCenter - rayOrigin; // Vector from rayOrigin to sphereCenter

	// Project e onto rayDirection
	Vector3 proj = rayDirection * (Vector3::Dot(e, rayDirection) / Vector3::LengthSq(rayDirection)); // ed

	float projLength = Vector3::Dot(e, rayDirection); // a

	//float bSquared = Vector3::Dot(e, e) - projLength * projLength; // b^2

	float f = sqrtf(sphereRadius * sphereRadius - Vector3::Dot(e, e) + projLength * projLength);
	cout << "f: " << f << endl;
	if (f < 0)
		return false; // Ray does not intersect sphere

	float t = projLength - f;
	cout << "t: " << t << endl;

	if (t > 0)
	{
		outIntersectionPoint = rayOrigin + rayDirection * t;

		cout << "inter point: " << outIntersectionPoint.x << " " <<
			outIntersectionPoint.y << " " <<
			outIntersectionPoint.z << endl;
		return true;
	}
		
	else
		return false;
	

}