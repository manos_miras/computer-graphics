#include "Splines.h"
#include <math.h>

Vector3 Lerp(const Vector3& a, const Vector3& b, float t)
{
	return a * t + b * (1.0f - t);
}

Vector3 QuadraticBezier(const vector<Vector3>& bezCurve, float t)
{
	return  (bezCurve[1] - bezCurve[0]) * 2 * (1 - t) + (bezCurve[2] - bezCurve[1]) * (2 * t);
}

Vector3 CubicBezier(const vector<Vector3>& bezCurve, float t)
{
	return (bezCurve[1] - bezCurve[0]) * 3 * ((1 - t) * (1 - t)) + (bezCurve[2] - bezCurve[1]) * 6 * (1 - t) * t + (bezCurve[3] - bezCurve[2]) * 3 * (t * t);
}

Vector3 CubicCatmullRom(const vector<Vector3>& controlPoints, float t)
{
	return Vector3();
}
