#include <iostream>
#include "Vector3.h"
#include "Splines.h"
#undef main

using namespace std;



int main()
{
	Vector3 cons1;
	int i;
	cout << "cons1 is " << cons1.x << ", " << cons1.y << ", " << cons1.z << ", " << endl;
	
	Vector3 x(3.0f, 2.0f, 8.0f);
	Vector3 x2(2.0f, 4.0f, 4.0f);
	cout << "x is " << x.x << ", " << x.y << ", " << x.z << ", " << endl;

	Vector3 y(x);
	cout << "y is " << y.x << ", " << y.y << ", " << y.z << ", " << endl;

	Vector3 add = x + y;
	cout << "add is x + y = " << add.x << ", " << add.y << ", " << add.z << ", " << endl;

	Vector3 sub = x - add;
	cout << "sub is x - add = " << sub.x << ", " << sub.y << ", " << sub.z << ", " << endl;

	Vector3 scalardiv = x / 4;
	cout << "scalardiv is x / 4 = " << scalardiv.x << ", " << scalardiv.y << ", " << scalardiv.z << ", " << endl;

	Vector3 scalarmul = x * 2;
	cout << "scalarmul is x * 2 = " << scalarmul.x << ", " << scalarmul.y << ", " << scalarmul.z << ", " << endl;

	x -= y;
	cout << "x -= y = " << x.x << ", " << x.y << ", " << x.z << ", " << endl;

	x += y;
	cout << "x += y = " << x.x << ", " << x.y << ", " << x.z << ", " << endl;

	x *= 5;
	cout << "x *= 5 = " << x.x << ", " << x.y << ", " << x.z << ", " << endl;

	x += -y;
	cout << "x += -y = " << x.x << ", " << x.y << ", " << x.z << ", " << endl;

	cout << "x = " << x.x << ", " << x.y << ", " << x.z << ", " << endl;

	cout << "y = " << y.x << ", " << y.y << ", " << y.z << ", " << endl;


	Vector3 cross = Vector3::Cross(x, y);

	cout << "Cross(x, y) = " << cross.x << ", " << cross.y << ", " << cross.z << ", " << endl;

	float dot = Vector3::Dot(x, y);

	cout << "Dot(x, y) = " << dot << endl;

	float length = Vector3::Length(x);

	cout << "Length(x) = " << length << endl;

	float lengthsq = Vector3::LengthSq(x);

	cout << "LengthSq(x) = " << lengthsq << endl;

	Vector3 normal = Vector3::Normalize(x);

	cout << "Normalize(x) = " << normal.x << ", " << normal.y << ", " << normal.z << ", " << endl;

	// SPLINES

	cout << "SPLINES" << endl;

	vector<Vector3> tsifsa
	{
		Vector3(1.0f, 3.0f, 0.0f), //p0
		Vector3(1.0f, 7.0f, 0.0f), //p1
		Vector3(4.0f, 7.0f, 0.0f) //p2
	};

	Vector3 res = Lerp(Vector3(1.0f, 2.0f, 0.0f), Vector3(1.0f, 5.0f, 0.0f),  0.75f);

	cout << "Lerp = " << res.x << ", " << res.y << ", " << res.z << ", " << endl;

	res = QuadraticBezier(tsifsa, 0.25f);
	
	cout << "QuadtraticBezier = " << res.x << ", " << res.y << ", " << res.z << ", " << endl;

	cin >> i;
	//int x = 0;
}

