#include "Vector3.h"
#include <cmath>

/*	A Vector3 class providing essential functionalities for Vector3 calculations.
	Made by Emmanuel Miras.															
	Last modified 23/01/2016 */

// CONSTRUCTOR
Vector3::Vector3()
{
	x = 0.0f;
	y = 0.0f;
	z = 0.0f;
}

// CONSTRUCTOR WITH ARGUMENTS
Vector3::Vector3(float x, float y, float z)
{
	this->x = x;
	this->y = y;
	this->z = z;
}

// COPY CONSTRUCTOR
Vector3::Vector3(const Vector3& rhs)
{
	x = rhs.x;
	y = rhs.y;
	z = rhs.z;
}

// += OPERATOR (ADDITION AND ASSIGNMENT)
void Vector3::operator+= (const Vector3 &v)
{
	x = x + v.x;
	y = y + v.y;
	z = z + v.z;
}

// -= OPERATOR (SUBTRACTION AND ASSIGNMENT)
void Vector3::operator-= (const Vector3 &v)
{
	x = x - v.x;
	y = y - v.y;
	z = z - v.z;
}

// *= OPERATOR (MULTIPLICATION AND ASSIGNMENT)
void Vector3::operator*= (const float    s)
{
	x = x * s;
	y = y * s;
	z = z * s;
}

// + OPERATOR (ADDITION)
Vector3 Vector3::operator+(const Vector3 &v) const
{
	Vector3 vec;
	vec.x = v.x + x;
	vec.y = v.y + y;
	vec.z = v.z + z;

	return vec;
}

// - OPERATOR (SUBTRACTION)
Vector3 Vector3::operator-(const Vector3 &v) const
{
	Vector3 vec;
	vec.x = x - v.x;
	vec.y = y - v.y;
	vec.z = z - v.z;

	return vec;
}

// / OPERATOR (DIVISION)
Vector3 Vector3::operator/(const float s) const
{
	Vector3 vec;
	vec.x = x / s;
	vec.y = y / s;
	vec.z = z / s;

	return vec;
}

// * OPERATOR (MULTIPLICATION)
Vector3 Vector3::operator*(const float s) const
{
	Vector3 vec;
	vec.x = x * s;
	vec.y = y * s;
	vec.z = z * s;

	return vec;
}

// Unary minus
Vector3 Vector3::operator- () const
{
	Vector3 unary;
	unary.x = x * -1;
	unary.y = y * -1;
	unary.z = z * -1;
	return unary;
}

// CROSS PRODUCT
Vector3 Vector3::Cross(const Vector3 &vA, const Vector3& vB)
{
	Vector3 cross;
	cross.x = vA.y * vB.z - vA.z * vB.y;
	cross.y = vA.z * vB.x - vA.x * vB.z;
	cross.z = vA.x * vB.y - vA.y * vB.x;
	return cross;
}

// DOT PRODUCT
float Vector3::Dot(const Vector3 &vA, const Vector3& vB)
{
	return vA.x * vB.x + vA.y * vB.y + vA.z * vB.z;
}

// NORMALISATION
Vector3 Vector3::Normalize(const Vector3& v)
{
	return v / Length(v);
}

// LENGTH AKA MAGNITUDE
float Vector3::Length(const Vector3& v)
{
	return sqrt(v.x * v.x + v.y * v.y + v.z * v.z);
}

// LENGTH SQUARED 
float Vector3::LengthSq(const Vector3& v)
{
	return(v.x * v.x + v.y * v.y + v.z * v.z);
}