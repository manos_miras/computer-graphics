#include "Matrix4.h"
#include <math.h>
#include <iostream>
using namespace std;

void Matrix4::printMatrix(const Matrix4& mat)
{
	cout << mat.m[0] << ", " << mat.m[4] << ", " << mat.m[8] << ", " << mat.m[12] << endl;
	cout << mat.m[1] << ", " << mat.m[5] << ", " << mat.m[9] << ", " << mat.m[13] << endl;
	cout << mat.m[2] << ", " << mat.m[6] << ", " << mat.m[10] << ", " << mat.m[14] << endl;
	cout << mat.m[3] << ", " << mat.m[7] << ", " << mat.m[11] << ", " << mat.m[15] << endl;
	cout << endl;
}

// Default Constructor
Matrix4::Matrix4()
{

}

// Copy Constructor
Matrix4::Matrix4(const Matrix4& rhs)
{
	for (int i = 0; i < 16; i++)
	{
		m[i] = rhs.m[i];
	}
}

// Constructor with float arguments
Matrix4::Matrix4(float _00, float _10, float _20, float _30,
	float _01, float _11, float _21, float _31,
	float _02, float _12, float _22, float _32,
	float _03, float _13, float _23, float _33)
{
	m[0] = _00;
	m[1] = _01;
	m[2] = _02;
	m[3] = _03;

	m[4] = _10;
	m[5] = _11;
	m[6] = _12;
	m[7] = _13;

	m[8] = _20;
	m[9] = _21;
	m[10] = _22;
	m[11] = _23;

	m[12] = _30;
	m[13] = _31;
	m[14] = _32;
	m[15] = _33;
}

// Zero Matrix
Matrix4 Matrix4::Zero()
{
	Matrix4 zero;

	// Loops through each float in the matrix and sets it to 0.0f
	for (int i = 0; i < 16; i++)
	{
		zero.m[i] = 0.0f;
	}

	return zero;
}

// Identity matrix
Matrix4 Matrix4::Identity()
{
	Matrix4 identity;
	// Set the diagonals of the matrix to 1.0f and the rest to 0.0f
	for (int i = 0; i < 16; i++)
	{
		if (i == 0 || i % 5 == 0)
			identity.m[i] = 1.0f;
		else
			identity.m[i] = 0.0f;
	}
	return identity;
}

// Transpose of Matrix
Matrix4 Matrix4::Transpose(const Matrix4& mat)
{

	Matrix4 transpose;
	// Keep the same value along the diagonals
	for (int i = 0; i < 16; i++)
	{
		if (i == 0 || i % 5 == 0)
			transpose.m[i] = mat.m[i];
	}
	// Transpose the rest
	transpose.m[1] = mat.m[4];
	transpose.m[2] = mat.m[8];
	transpose.m[3] = mat.m[12];
	transpose.m[4] = mat.m[1];
	transpose.m[6] = mat.m[9];
	transpose.m[7] = mat.m[13];
	transpose.m[8] = mat.m[2];
	transpose.m[9] = mat.m[6];
	transpose.m[11] = mat.m[14];
	transpose.m[12] = mat.m[3];
	transpose.m[13] = mat.m[7];
	transpose.m[14] = mat.m[11];

	return transpose;

}

// Sets the Scale Matrix
Matrix4 Matrix4::SetScale(const Vector3& scale)
{
	// Set to identity
	Matrix4 scaled = Identity();

	scaled.m[0] *= scale.x;
	scaled.m[5] *= scale.y;
	scaled.m[10] *= scale.z;

	return scaled;
}

// Sets the Translation Matirx
Matrix4 Matrix4::SetTranslation(const Vector3& translation)
{
	// Set to identity
	Matrix4 translated = Identity();

	translated.m[12] = translation.x;
	translated.m[13] = translation.y;
	translated.m[14] = translation.z;

	return translated;
}

// Gets the vector translation
Vector3 Matrix4::GetTranslation(const Matrix4& mat)
{
	return(Vector3(mat.m[12], mat.m[13], mat.m[14]));
}

// [] operator
float& Matrix4::operator[] (int index)
{
	return m[index];
}

// [] operator
const float& Matrix4::operator[] (int index) const
{
	return m[index];
}

// Sets the Rotation Matrix
Matrix4 Matrix4::SetRotationAxis(const Vector3& axis, float angle)
{
	Matrix4 rotation = Identity();

	float c = cos(angle);
	float s = sin(angle);


	if (axis.x == 1.0f)
	{
		rotation.m[5] = c;
		rotation.m[6] = s;
		rotation.m[9] = -s;
		rotation.m[10] = c;
	}

	if (axis.y == 1.0f)
	{
		rotation.m[0] = c;
		rotation.m[2] = -s;
		rotation.m[8] = s;
		rotation.m[10] = c;
	}

	if (axis.z == 1.0f)
	{
		rotation.m[0] = c;
		rotation.m[1] = s;
		rotation.m[4] = -s;
		rotation.m[5] = c;
	}

	return rotation;
}

// Transforms a point in 3D Space
Vector3 Matrix4::TransformPoint(const Matrix4& mat, const Vector3& p)
{
	Vector3 point;

	point.x = mat.m[0] * p.x + mat.m[4] * p.y + mat.m[8] * p.z + mat.m[12];
	point.y = mat.m[1] * p.x + mat.m[5] * p.y + mat.m[9] * p.z + mat.m[13];
	point.z = mat.m[2] * p.x + mat.m[6] * p.y + mat.m[10] * p.z + mat.m[14];

	return point;
}

// Transforms a direction in 3D Space
Vector3 Matrix4::TransformDirection(const Matrix4& mat, const Vector3& n)
{
	Vector3 direction;

	direction.x = mat.m[0] * n.x + mat.m[4] * n.y + mat.m[8] * n.z;
	direction.y = mat.m[1] * n.x + mat.m[5] * n.y + mat.m[9] * n.z;
	direction.z = mat.m[2] * n.x + mat.m[6] * n.y + mat.m[10] * n.z;

	return direction;
}

// Operator for multiplication
Matrix4 operator*(const Matrix4& lhs, const Matrix4& rhs)
{
	Matrix4 product;

	// Row 1
	product.m[0] = lhs.m[0] * rhs.m[0] + lhs.m[4] * rhs.m[1] + lhs.m[8] * rhs.m[2] + lhs.m[12] * rhs.m[3];
	product.m[4] = lhs.m[0] * rhs.m[4] + lhs.m[4] * rhs.m[5] + lhs.m[8] * rhs.m[6] + lhs.m[12] * rhs.m[7];
	product.m[8] = lhs.m[0] * rhs.m[8] + lhs.m[4] * rhs.m[9] + lhs.m[8] * rhs.m[10] + lhs.m[12] * rhs.m[11];
	product.m[12] = lhs.m[0] * rhs.m[12] + lhs.m[4] * rhs.m[13] + lhs.m[8] * rhs.m[14] + lhs.m[12] * rhs.m[15];

	// Row 2
	product.m[1] = lhs.m[1] * rhs.m[0] + lhs.m[5] * rhs.m[1] + lhs.m[9] * rhs.m[2] + lhs.m[13] * rhs.m[3];
	product.m[5] = lhs.m[1] * rhs.m[4] + lhs.m[5] * rhs.m[5] + lhs.m[9] * rhs.m[6] + lhs.m[13] * rhs.m[7];
	product.m[9] = lhs.m[1] * rhs.m[8] + lhs.m[5] * rhs.m[9] + lhs.m[9] * rhs.m[10] + lhs.m[13] * rhs.m[11];
	product.m[13] = lhs.m[1] * rhs.m[12] + lhs.m[5] * rhs.m[13] + lhs.m[9] * rhs.m[14] + lhs.m[13] * rhs.m[15];

	// Row 3
	product.m[2] = lhs.m[2] * rhs.m[0] + lhs.m[6] * rhs.m[1] + lhs.m[10] * rhs.m[2] + lhs.m[14] * rhs.m[3];
	product.m[6] = lhs.m[2] * rhs.m[4] + lhs.m[6] * rhs.m[5] + lhs.m[10] * rhs.m[6] + lhs.m[14] * rhs.m[7];
	product.m[10] = lhs.m[2] * rhs.m[8] + lhs.m[6] * rhs.m[9] + lhs.m[10] * rhs.m[10] + lhs.m[14] * rhs.m[11];
	product.m[14] = lhs.m[2] * rhs.m[12] + lhs.m[6] * rhs.m[13] + lhs.m[10] * rhs.m[14] + lhs.m[14] * rhs.m[15];

	// Row 4
	product.m[3] = lhs.m[3] * rhs.m[0] + lhs.m[7] * rhs.m[1] + lhs.m[11] * rhs.m[2] + lhs.m[15] * rhs.m[3];
	product.m[7] = lhs.m[3] * rhs.m[4] + lhs.m[7] * rhs.m[5] + lhs.m[11] * rhs.m[6] + lhs.m[15] * rhs.m[7];
	product.m[11] = lhs.m[3] * rhs.m[8] + lhs.m[7] * rhs.m[9] + lhs.m[11] * rhs.m[10] + lhs.m[15] * rhs.m[11];
	product.m[15] = lhs.m[3] * rhs.m[12] + lhs.m[7] * rhs.m[13] + lhs.m[11] * rhs.m[14] + lhs.m[15] * rhs.m[15];

	return product;
}