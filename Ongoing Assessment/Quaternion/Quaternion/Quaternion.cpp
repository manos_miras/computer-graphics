#include "Quaternion.h"
#include <math.h>

/*	
	A Quaternion class providing essential functionalities for Quaternion calculations.
	Made by Emmanuel Miras.
	Last modified 10/02/2016 
*/

// Quaternion Constructor with optional parameteres
Quaternion::Quaternion(float xx, float yy, float zz, float ww)
{
	x = xx;
	y = yy;
	z = zz;
	w = ww;
}

// Copy constructor
Quaternion::Quaternion(const Quaternion& rhs)
{
	x = rhs.x;
	y = rhs.y;
	z = rhs.z;
	w = rhs.w;
}

// Quaternion Identity
Quaternion Quaternion::Identity()
{
	Quaternion identity;

	identity.x = 0.0f;
	identity.y = 0.0f;
	identity.z = 0.0f;
				 
	identity.w = 1.0f;

	return identity;
}

// Quaternion Conjugate
Quaternion Quaternion::Conjugate(const Quaternion& q)
{
	Quaternion conjugate;

	conjugate.w = q.w;
	conjugate.x = -q.x;
	conjugate.y = -q.y;
	conjugate.z = -q.z;

	return conjugate;
}

// Quaternion Length/Magnitude
float Quaternion::Length(const Quaternion& q)
{
	return sqrt(q.x * q.x + q.y * q.y + q.z * q.z + q.w * q.w);
}

// Quaternion Length/Magnitude Squared
float Quaternion::LengthSq(const Quaternion& q)
{
	return q.x * q.x + q.y * q.y + q.z * q.z + q.w * q.w;
}

// Quaternion Dot Product
float Quaternion::Dot(const Quaternion& q1, const Quaternion& q2)
{
	return q1.x * q2.x + q1.y * q2.y + q1.z * q2.z + q1.w * q2.w;
}

// Quaternion Normal
Quaternion Quaternion::Normalize(const Quaternion& q)
{
	//return q / Length(q);
	Quaternion normal;

	float len = Length(q);
	// Check for division by zero
	if (len > 0)
	{
		normal.w = q.w / len;
		normal.x = q.x / len;
		normal.y = q.y / len;
		normal.z = q.z / len;
	}
	else
		normal = Identity();
	return normal;
}

// Assignment operator overload
Quaternion& Quaternion::operator = (const Quaternion& rhs)
{
	x = rhs.x;
	y = rhs.y;
	z = rhs.z;
	w = rhs.w;

	return *this;
}

// Axis Angle to Quaternion
Quaternion Quaternion::FromAxisAngle(const Vector3& v, float angle)
{
	Quaternion q;
	float angleOver2 = angle * 0.5f;
	float sinAngleOver2 = sin(angleOver2);

	// Set the values
	q.w = cos(angleOver2);
	q.x = v.x * sinAngleOver2;
	q.y = v.y * sinAngleOver2;
	q.z = v.z * sinAngleOver2;

	return q;
}

// Quaternion to Axis Angle
float Quaternion::ToAxisAngle(const Quaternion& q, Vector3& v)
{
	v.x = q.x / sqrt(1 - q.w * q.w);
	v.y = q.y / sqrt(1 - q.w * q.w);
	v.z = q.z / sqrt(1 - q.w * q.w);
	return 2 * acos(q.w);
}

// Quaternion slerp
Quaternion Quaternion::Slerp(float t, const Quaternion& p, const Quaternion& q)
{
	Quaternion slerp;

	float cosOmega = Dot(p, q); // Compute cosine of the angle between quaternions
	float sinOmega = sqrt(1.0f - cosOmega * cosOmega); // Compute sin of the angle
	float omega = atan2(sinOmega, cosOmega); // Compute angle
	float oneOverSinOmega = 1.0f / sinOmega;

	// Interpolation parameters
	float k0 = sin((1.0f - t) * omega) * oneOverSinOmega;
	float k1 = sin(t * omega) *oneOverSinOmega;

	// Interpolate
	slerp.w = p.w * k0 + q.w * k1;
	slerp.x = p.x * k0 + q.x * k1;
	slerp.y = p.y * k0 + q.y * k1;
	slerp.z = p.z * k0 + q.z * k1;

	return slerp;
}

// Matrix to Quaternion
Quaternion Quaternion::FromRotationMatrix(const Matrix4& m)
{
	Quaternion q;

	// Determine which of w, x, y, z has the largest absolute value

	float fourWSquaredMinus1 = m.m[0] + m.m[5] + m.m[10];
	float fourXSquaredMinus1 = m.m[0] - m.m[5] - m.m[10];
	float fourYSquaredMinus1 = m.m[5] - m.m[0] - m.m[10];
	float fourZSquaredMinus1 = m.m[10] - m.m[0] - m.m[5];

	// Set w to biggest
	float fourBiggestSquaredMinus1 = fourWSquaredMinus1;
	int biggestIndex = 0;


	if (fourXSquaredMinus1 > fourBiggestSquaredMinus1)
	{
		// Set x to biggest
		fourBiggestSquaredMinus1 = fourXSquaredMinus1;
		biggestIndex = 1;
	}

	if (fourYSquaredMinus1 > fourBiggestSquaredMinus1)
	{
		// Set y to biggest
		fourBiggestSquaredMinus1 = fourYSquaredMinus1;
		biggestIndex = 2;
	}

	if (fourZSquaredMinus1 > fourBiggestSquaredMinus1)
	{
		// Set z to biggest
		fourBiggestSquaredMinus1 = fourZSquaredMinus1;
		biggestIndex = 3;
	}

	// Perform square root and division

	float biggestVal = sqrt(fourBiggestSquaredMinus1 + 1.0f) * 0.5f;
	float mult = 0.25f / biggestVal;

	// Apply table to compute quaternion values

	switch (biggestIndex)
	{
		case 0:
			q.w = biggestVal;
			q.x = (m.m[9] - m.m[6]) * mult;
			q.y = (m.m[2] - m.m[8]) * mult;
			q.z = (m.m[4] - m.m[1]) * mult;
			break;
		case 1:
			q.x = biggestVal;
			q.w = (m.m[9] - m.m[6]) * mult;
			q.y = (m.m[4] + m.m[1]) * mult;
			q.z = (m.m[2] + m.m[8]) * mult;
			break;
		case 2:
			q.y = biggestVal;
			q.w = (m.m[2] - m.m[8]) * mult;
			q.x = (m.m[4] + m.m[1]) * mult;
			q.z = (m.m[9] + m.m[6]) * mult;
			break;
		case 3:
			q.z = biggestVal;
			q.w = (m.m[4] - m.m[1]) * mult;
			q.x = (m.m[9] + m.m[6]) * mult;
			q.y = (m.m[9] + m.m[6]) * mult;
			break;
	}
	//q.w = sqrt(m.m[0] + m.m[4] + m.m[8] + 1) / 2;
	//q.x = (m.m[9] - m.m[6]) / (4 * q.w);
	//q.y = (m.m[2] - m.m[8]) / (4 * q.w);
	//q.z = (m.m[4] - m.m[1]) / (4 * q.w);

	/*
	* Column-major 4x4 matrix
	*
	* Layout:
	*			0  4  8  12
	*			1  5  9  13
	*			2  6  10 14
	*			3  7  11 15
	*/
	return q;
}

// Quartanion to Rotation Matrix conversion
Matrix4 Quaternion::ToRotationMatrix(const Quaternion& q)
{
	Matrix4 mat4;
	mat4.Identity();

	mat4.m[0] = 1 - 2 * (q.y * q.y) - 2 * (q.z * q.z);
	mat4.m[1] = 2 * (q.x * q.y) - 2 * (q.w * q.z);
	mat4.m[2] = 2 * (q.x * q.z) + 2 * (q.w * q.y);

	mat4.m[4] = 2 * (q.x * q.y) + 2 * (q.w * q.z);
	mat4.m[5] = 1 - 2 * (q.x * q.x) - 2 * (q.z * q.z);
	mat4.m[6] = 2 * (q.y * q.z) - 2 * (q.w * q.x);

	mat4.m[8] = 2 * (q.x * q.z) - 2 * (q.w * q.y);
	mat4.m[9] = 2 * (q.y * q.z) + 2 * (q.w * q.x);
	mat4.m[10] = 1 - 2 * (q.x * q.x) - 2 * (q.y * q.y);

	return mat4;
}

// Quaternion scalar multiplication
Quaternion operator*(const Quaternion& q, float s)
{
	Quaternion scaled;

	scaled.x = q.x * s;
	scaled.y = q.y * s;
	scaled.z = q.z * s;
	scaled.w = q.w * s;

	return scaled;
}

// Quaternion scalar multiplication
Quaternion operator*(float s, const Quaternion& q)
{
	Quaternion scaled;

	scaled.x = q.x * s;
	scaled.y = q.y * s;
	scaled.z = q.z * s;
	scaled.w = q.w * s;

	return scaled;
}

// Quaternion addition
Quaternion operator+(const Quaternion& q1, const Quaternion& q2)
{
	Quaternion sum;

	sum.x = q1.x + q2.x;
	sum.y = q1.y + q2.y;
	sum.z = q1.z + q2.z;
	sum.w = q1.w + q2.w;

	return sum;
}

// Quaternion cross product
Quaternion operator*(const Quaternion& q1, const Quaternion& q2)
{
	Quaternion cross;

	// Vector 1
	Vector3 vec3a(q2.x, q2.y, q2.z);
	// Vector 2
	Vector3 vec3b(q1.x, q1.y, q1.z);

	// Set Scalar component
	cross.w = (q1.w * q2.w) - Vector3::Dot(vec3a, vec3b);

	// Calculate 1st Vector
	vec3a *= q1.w;

	// Calculate 2nd Vector
	vec3b *= q2.w;

	// Calculate 3rd Vector
	Vector3 vec3c = Vector3::Cross(Vector3(q1.x, q1.y, q1.z), Vector3(q2.x, q2.y, q2.z));

	// Calculate sum of Vectors
	Vector3 sum = vec3a + vec3b + vec3c;

	// Set Quaternion Vector component
	cross.x = sum.x;
	cross.y = sum.y;
	cross.z = sum.z;

	return cross;
}