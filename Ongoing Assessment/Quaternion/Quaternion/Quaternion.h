#pragma once


// Minimal Quaternion class


#include "Vector3.h"
#include "Matrix4.h"


class Quaternion
{
public:
	float x, y, z, w;

	Quaternion(float xx = 0, float yy = 0, float zz = 0, float ww = 1); // done
	Quaternion(const Quaternion& rhs); //done

	Quaternion& operator = (const Quaternion& rhs); //done

	// convenience function for the identity quaternion
	static Quaternion 	Identity(); // done
	static Quaternion 	Conjugate(const Quaternion& q); // done

	static float 		Length(const Quaternion& q); // done
	static float 		LengthSq(const Quaternion& q); // done
	static Quaternion 	Normalize(const Quaternion& q); // done?
	static float 		Dot(const Quaternion& q1, const Quaternion& q2); // done

	static Quaternion 	FromAxisAngle(const Vector3&    v, float angle); // done
	static float 	  	ToAxisAngle(const Quaternion& q, Vector3& v); // done??????

	static Matrix4 		ToRotationMatrix(const Quaternion& q); // done
	static Quaternion 	FromRotationMatrix(const Matrix4&    m); // done

	static Quaternion 	Slerp(float t, const Quaternion& p, const Quaternion& q); // done

}; // End Quaternion(...)



Quaternion operator*(const Quaternion& q1, const Quaternion& q2); // done

Quaternion operator*(const Quaternion& q, float s); // done

Quaternion operator*(float s, const Quaternion& q); // done

Quaternion operator+(const Quaternion& q1, const Quaternion& q2); // done

// TODO: TEST EVERYTHING