#version 440

// MVP transformation matrix
uniform mat4 MVP;
// M transformation matrix
uniform mat4 M;
// N transformation matrix
uniform mat3 N;
// Eye position
uniform vec3 eye_pos;

// Incoming position
layout (location = 0) in vec3 position;
// Incoming normal
layout (location = 2) in vec3 normal;

// Outgoing 3D texture coordinate
layout (location = 0) out vec3 tex_coord;

void main()
{
	// Calculate screen position
	gl_Position = MVP * vec4(position, 1.0);
	// ************************
	// Calculate world position
	// ************************
	vec3 worldPosition = vec3(M * vec4(position, 1.0f));
	// ********************
	// Transform the normal
	// ********************
	vec3 transformed_normal = normal * N;
	// ****************************************************************
	// Calculate tex_coord using world position and transformed normal
	// ****************************************************************
	vec3 view_dir = (eye_pos - worldPosition) / length(eye_pos - worldPosition);

	//vec3 half_vector = normalize(worldPosition + transformed_normal);	
	tex_coord = normalize(worldPosition + transformed_normal);

	//tex_coord = view_dir * dot(normal, half_vector);

	//vec4 specular = (mat.specular_reflection * light_colour) * pow(max(dot(normal, half_vector), 0), mat.shininess);

	 
}