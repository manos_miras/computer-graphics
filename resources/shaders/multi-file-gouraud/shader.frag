#version 440

// Texture to sample from
uniform sampler2D tex;

// Incoming primary colour
layout (location = 0) in vec4 primary;
// Incoming secondary colour
layout (location = 1) in vec4 secondary;
// Incoming texture coordinate
layout (location = 2) in vec2 tex_coord;

// Outgoing colour
layout (location = 0) out vec4 colour;

void main()
{
	// **************
	// Sample texture
	// **************
	vec4 tex_colour = texture(tex, tex_coord);

	// ****************
	// Calculate colour
	// ****************
	colour = primary * tex_colour + secondary;
	colour.a = 1.0f;
}





//#version 440

//// Model transformation matrix
//uniform mat4 M;
//// Transformation matrix
//uniform mat4 MVP;
//// Normal matrix
//uniform mat3 N;

//// Incoming position
//layout (location = 0) in vec3 position;
//// Incoming normal
//layout (location = 2) in vec3 input_normal;
//// Incoming texture coordinate
//layout (location = 10) in vec2 tex_coord_in;

//// Outgoing position
//layout (location = 0) out vec3 vertex_position;
//// Outgoing transformed normal
//layout (location = 1) out vec3 transformed_normal;
//// Outgoing texture coordinate
//layout (location = 2) out vec2 tex_coord_out;

//void main()
//{
//	vec3 normal = normalize(input_normal);
//	// Calculate screen position
//	gl_Position = MVP * vec4(position, 1.0);
//	// **************************************
//	// Output other values to fragment shader
//	// **************************************
//	vertex_position = vec3(M * vec4(position, 1.0f));
//	transformed_normal = N * normal;
//	tex_coord_out = tex_coord_in;
//}