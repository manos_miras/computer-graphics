#version 440

// This shader requires direction.frag, point.frag and spot.frag

// Directional light structure
#ifndef DIRECTIONAL_LIGHT
#define DIRECTIONAL_LIGHT
struct directional_light
{
	vec4 ambient_intensity;
	vec4 light_colour;
	vec3 light_dir;
};
#endif

// Point light information
#ifndef POINT_LIGHT
#define POINT_LIGHT
struct point_light
{
	vec4 light_colour;
	vec3 position;
	float constant;
	float linear;
	float quadratic;
};
#endif

// Spot light data
#ifndef SPOT_LIGHT
#define SPOT_LIGHT
struct spot_light
{
	vec4 light_colour;
	vec3 position;
	vec3 direction;
	float constant;
	float linear;
	float quadratic;
	float power;
};
#endif

// A material structure
#ifndef MATERIAL
#define MATERIAL
struct material
{
	vec4 emissive;
	vec4 diffuse_reflection;
	vec4 specular_reflection;
	float shininess;
};
#endif

// Forward declarations of used functions
vec4 calculate_direction_primary(in directional_light light, in material mat, in vec3 normal, in vec3 view_dir);
vec4 calculate_direction_secondary(in directional_light light, in material mat, in vec3 normal, in vec3 view_dir);

vec4 calculate_point_primary(in point_light point, in material mat, in vec3 position, in vec3 normal, in vec3 view_dir);
vec4 calculate_point_secondary(in point_light point, in material mat, in vec3 position, in vec3 normal, in vec3 view_dir);

vec4 calculate_spot_primary(in spot_light spot, in material mat, in vec3 position, in vec3 normal, in vec3 view_dir);
vec4 calculate_spot_secondary(in spot_light spot, in material mat, in vec3 position, in vec3 normal, in vec3 view_dir);

//// Directional light information
//uniform directional_light light;
//// Point lights being used in the scene
uniform point_light points[4];
// Spot lights being used in the scene
uniform spot_light spots[5];
// Material of the object being rendered
//uniform material mat;
//// Position of the eye
//uniform vec3 eye_pos;
//// Texture to sample from
//uniform sampler2D tex;

//// Incoming position
//layout (location = 0) in vec3 position;
//// Incoming normal
//layout (location = 1) in vec3 normal;
//// Incoming texture coordinate
//layout (location = 2) in vec2 tex_coord;

//// Outgoing colour
//layout (location = 0) out vec4 colour;


// The model matrix
uniform mat4 M;
// The transformation matrix
uniform mat4 MVP;
// The normal matrix
uniform mat3 N;
// Directional light for the scene
uniform directional_light light;
// Material of the object
uniform material mat;
// Position of the camera
uniform vec3 eye_pos;

// Incoming position
layout (location = 0) in vec3 position;
// Incoming normal
layout (location = 2) in vec3 input_normal;
// Incoming texture coordinate
layout (location = 10) in vec2 tex_coord_in;

// Outgoing primary colour
layout (location = 0) out vec4 primary;
// Outgoing secondary colour
layout (location = 1) out vec4 secondary;
// Outgoing texture coordinate
layout (location = 2) out vec2 tex_coord_out;



void main()
{
	vec3 normal = normalize(input_normal);
	gl_Position = MVP * vec4(position, 1.0f);

	vec3 transformed_normal = N * normal;

	primary = vec4(0.0, 0.0, 0.0, 1.0);
	secondary = vec4(0.0, 0.0, 0.0, 1.0);
	// ************************
	// Calculate view direction
	// ************************
	// **********************************
	// Calculate world position of vertex
	// **********************************
	vec4 worldPosition = M * vec4(position, 1.0f);
	vec3 worldPositionFinal = vec3(worldPosition);

	vec3 view_dir = normalize(eye_pos - worldPositionFinal);
	// **************
	// Sample texture
	// **************
	//vec4 tex_colour = texture(tex, tex_coord);
	// **********************************
	// Calculate directional light colour
	// **********************************
	primary = calculate_direction_primary(light, mat, transformed_normal, view_dir);
	secondary = calculate_direction_secondary(light, mat, transformed_normal, view_dir);
	// ****************
	// Sum point lights
	// ****************
	for (int i = 0; i < 4; ++i)
	{
		primary += calculate_point_primary(points[i], mat, worldPositionFinal, transformed_normal, view_dir);
		secondary += calculate_point_secondary(points[i], mat, worldPositionFinal, transformed_normal, view_dir);

	}
	// ***************
	// Sum spot lights
	// ***************
	for (int i = 0; i < 5; ++i)
	{
		primary += calculate_spot_primary(spots[i], mat, worldPositionFinal, transformed_normal, view_dir);
		secondary += calculate_spot_secondary(spots[i], mat, worldPositionFinal, transformed_normal, view_dir);
	}

	tex_coord_out = tex_coord_in;
}

//// ******************
//	// Calculate position
//	// ******************
//	gl_Position = MVP * vec4(position, 1.0f);

//	// ***************************
//	// Calculate ambient component
//	// ***************************
//	vec4 ambient = light.ambient_intensity * mat.diffuse_reflection;

//	// ********************
//	// Transform the normal
//	// ********************
//	vec3 transformed_normal = N * normal;

//	// ***************************
//	// Calculate diffuse component
//	// ***************************
//	// Calculate k
//	const float k = max(dot(transformed_normal, light.light_dir), 0.0f);
//	// Calculate diffuse
//	vec4 diffuse = k * (mat.diffuse_reflection * light.light_colour);
	

//	// **********************************
//	// Calculate world position of vertex
//	// **********************************
//	vec4 worldPosition = M * vec4(position, 1.0f);
//	vec3 worldPositionFinal = vec3(worldPosition);

//	// ************************
//	// Calculate view direction
//	// ************************
//	vec3 view_dir = (eye_pos - worldPositionFinal) / length(eye_pos - worldPositionFinal);

//	// ****************************************************
//	// Calculate half vector between view_dir and light_dir
//	// ****************************************************
//	vec3 half_vector = (light.light_dir + view_dir) / length(light.light_dir + view_dir);

//	// ****************************
//	// Calculate specular component
//	// ****************************
//	// Calculate k
//	const float k2 = pow(max(dot(transformed_normal, vec3(half_vector)), 0.0f), mat.shininess);
//	// Calculate specular
//	vec4 specular = k2 * (mat.diffuse_reflection * light.light_colour);
	

//	// ***********
//	// Set primary
//	// ***********
//	primary = mat.emissive + ambient + diffuse;

//	// *************
//	// Set secondary
//	// *************
//	secondary = specular;

//	// *****************************************
//	// Ensure primary and secondary alphas are 1
//	// *****************************************
//	primary.a = 1.0f;
//	secondary.a = 1.0f;

//	// *******************************
//	// Pass through texture coordinate
//	// *******************************
//	tex_coord_out = tex_coord_in;