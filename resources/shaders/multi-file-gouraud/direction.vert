// Directional light structure
#ifndef DIRECTIONAL_LIGHT
#define DIRECTIONAL_LIGHT
struct directional_light
{
	vec4 ambient_intensity;
	vec4 light_colour;
	vec3 light_dir;
};
#endif

// A material structure
#ifndef MATERIAL
#define MATERIAL
struct material
{
	vec4 emissive;
	vec4 diffuse_reflection;
	vec4 specular_reflection;
	float shininess;
};
#endif

vec4 calculate_direction_primary(in directional_light light, in material mat, in vec3 normal, in vec3 view_dir)//, in vec4 tex_colour)
{
	vec3 normalized_normal = normalize(normal);
	// Calculate ambient component
	vec4 ambient = light.ambient_intensity * mat.diffuse_reflection;

	// **** Calculate diffuse component ****

	// Calculate k
	float k = max(dot(normalized_normal, light.light_dir), 0.0f);

	// Calculate diffuse
	vec4 diffuse = k * (mat.diffuse_reflection * light.light_colour);

	// Calculate half vector between view_dir and light_dir
	vec3 half_vector = normalize(light.light_dir + view_dir);

	// **** Calculate specular component ****

	// Calculate k
	float k2 = pow(max(dot(normalized_normal, vec3(half_vector)), 0.0f), mat.shininess);

	// Calculate specular
	vec4 specular = k2 * (mat.diffuse_reflection * light.light_colour);
	
	// Set primary colour
	vec4 colour = mat.emissive + ambient + diffuse;

	// Ensure colour alpha is 1
	colour.a = 1.0f;

	// Return colour
	return colour;
}

vec4 calculate_direction_secondary(in directional_light light, in material mat, in vec3 normal, in vec3 view_dir)//, in vec4 tex_colour)
{
	vec3 normalized_normal = normalize(normal);
	// Calculate ambient component
	vec4 ambient = light.ambient_intensity * mat.diffuse_reflection;

	// **** Calculate diffuse component ****

	// Calculate k
	float k = max(dot(normalized_normal, light.light_dir), 0.0f);

	// Calculate diffuse
	vec4 diffuse = k * (mat.diffuse_reflection * light.light_colour);

	// Calculate half vector between view_dir and light_dir
	vec3 half_vector = normalize(light.light_dir + view_dir);

	// **** Calculate specular component ****

	// Calculate k
	float k2 = pow(max(dot(normalized_normal, vec3(half_vector)), 0.0f), mat.shininess);

	// Calculate specular
	vec4 specular = k2 * (mat.diffuse_reflection * light.light_colour);
	
	// Set primary colour
	vec4 colour = specular;

	// Ensure colour alpha is 1
	colour.a = 1.0f;

	// Return colour
	return colour;
}