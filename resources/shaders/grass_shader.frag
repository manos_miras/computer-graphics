#version 440

//Dissolve factor value
uniform float dissolve_factor;
uniform sampler2D dissolve;

// Texture to use on billboards
uniform sampler2D tex;

// Incoming texture coordinate
layout (location = 0) in vec2 tex_coord;

// Outgoing colour
layout (location = 0) out vec4 colour;

void main()
{
	// Get dissolve value from the dissolve texture
	vec4 dissolve_value = texture(dissolve, tex_coord);

	// If b component is greater than dissolve factor, discard
	if (dissolve_value.b > dissolve_factor)
		discard;

	// Get texture colour
	colour = texture(tex, tex_coord);
}