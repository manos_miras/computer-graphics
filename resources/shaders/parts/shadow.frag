// Calculates the shadow factor of a vertex
float calculate_shadow(in sampler2D shadow_map, in vec4 light_space_pos)
{
	const float Epsilon = 0.0000001f;
	// *********************************
    // Get light screen space coordinate
	// *********************************
	vec3 proj_coords = light_space_pos.xyz / light_space_pos.w;

	vec2 shadow_tex_coords;
	// ********************************************************
    // Use this to calculate texture coordinates for shadow map
	// ********************************************************
    // This is a bias calculation to convert to texture space
	shadow_tex_coords.x = (0.5f * proj_coords.x) + 0.5f;
	shadow_tex_coords.y = (0.5f * proj_coords.y) + 0.5f;


	/*
	
	loop{
		vec2 offset = vec2(0.01f * x, 0.01f*y);
		float depth = texture(shadow_map, shadow_tex_coords + offset).x;
	}
	
	*/

	// Check shadow coord is in range
	if (shadow_tex_coords.x < 0.0f || shadow_tex_coords.x > 1.0f || shadow_tex_coords.y < 0.0f || shadow_tex_coords.y > 1.0f)
		return 1.0;

	// ***************************
    // Interested in depths 0 to 1
	// ***************************
	
	float z = (0.5f * proj_coords.z) + 0.5f;

	// *************************
    // Now sample the shadow map
	// *************************
	float depth = texture(shadow_map, shadow_tex_coords).x;

	// ***************************************************************
    // Check if depth is in range.  Add a slight epsilon for precision
	// ***************************************************************
	if (depth == 0.0f)
		return 1.0f;
	else if (depth < z + Epsilon)
		return 0.5f; // In shade
	else 
		return 1.0f; // Not in shade
    
}