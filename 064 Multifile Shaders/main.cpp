#include <graphics_framework.h>
#include <glm\glm.hpp>

using namespace std;
using namespace graphics_framework;
using namespace glm;

map<string, mesh> meshes;
effect phong_eff;
effect gouraud_eff;
texture tex;
target_camera cam;
directional_light light;
vector<point_light> points(4);
vector<spot_light> spots(5);
string lightingType = "phong";

bool load_content()
{
    // Create plane mesh
    meshes["plane"] = mesh(geometry_builder::create_plane());

    // Create scene
    meshes["box"] = mesh(geometry_builder::create_box());
    meshes["tetra"] = mesh(geometry_builder::create_tetrahedron());
    meshes["pyramid"] = mesh(geometry_builder::create_pyramid());
    meshes["disk"] = mesh(geometry_builder::create_disk(20));
    meshes["cylinder"] = mesh(geometry_builder::create_cylinder(20, 20));
    meshes["sphere"] = mesh(geometry_builder::create_sphere(20, 20));
    meshes["torus"] = mesh(geometry_builder::create_torus(20, 20, 1.0f, 5.0f));

    // Transform objects
    meshes["box"].get_transform().scale = vec3(5.0f, 5.0f, 5.0f);
    meshes["box"].get_transform().translate(vec3(-10.0f, 2.5f, -30.0f));
    meshes["tetra"].get_transform().scale = vec3(4.0f, 4.0f, 4.0f);
    meshes["tetra"].get_transform().translate(vec3(-30.0f, 10.0f, -10.0f));
    meshes["pyramid"].get_transform().scale = vec3(5.0f, 5.0f, 5.0f);
    meshes["pyramid"].get_transform().translate(vec3(-10.0f, 7.5f, -30.0f));
    meshes["disk"].get_transform().scale = vec3(3.0f, 1.0f, 3.0f);
    meshes["disk"].get_transform().translate(vec3(-10.0f, 11.5f, -30.0f));
    meshes["disk"].get_transform().rotate(vec3(half_pi<float>(), 0.0f, 0.0f));
    meshes["cylinder"].get_transform().scale = vec3(5.0f, 5.0f, 5.0f);
    meshes["cylinder"].get_transform().translate(vec3(-25.0f, 2.5f, -25.0f));
    meshes["sphere"].get_transform().scale = vec3(2.5f, 2.5f, 2.5f);
    meshes["sphere"].get_transform().translate(vec3(-25.0f, 10.0f, -25.0f));
    meshes["torus"].get_transform().translate(vec3(-25.0f, 10.0f, -25.0f));
    meshes["torus"].get_transform().rotate(vec3(half_pi<float>(), 0.0f, 0.0f));

    // ***********************
    // Set materials
    // - all emissive is black
    // - all specular is white
    // - all shininess is 25
    // ***********************
	const vec4 emissive(0.0f, 0.0f, 0.0f, 1.0f);
	const vec4 specular(1.0f, 1.0f, 1.0f, 1.0f);
	const float shininess = 25.0f;

	// Red box
	meshes["box"].get_material().set_diffuse(vec4(1.0f, 0.0f, 0.0f, 1.0f));
	meshes["box"].get_material().set_emissive(emissive);
	meshes["box"].get_material().set_specular(specular);
	meshes["box"].get_material().set_shininess(shininess);
	// Green tetra
	meshes["tetra"].get_material().set_diffuse(vec4(0.0f, 1.0f, 0.0f, 1.0f));
	meshes["tetra"].get_material().set_emissive(emissive);
	meshes["tetra"].get_material().set_specular(specular);
	meshes["tetra"].get_material().set_shininess(shininess);
	// Blue pyramid
	meshes["pyramid"].get_material().set_diffuse(vec4(0.0f, 0.0f, 1.0f, 1.0f));
	meshes["pyramid"].get_material().set_emissive(emissive);
	meshes["pyramid"].get_material().set_specular(specular);
	meshes["pyramid"].get_material().set_shininess(shininess);
	// Yellow disk
	meshes["disk"].get_material().set_diffuse(vec4(1.0f, 1.0f, 0.0f, 1.0f));
	meshes["disk"].get_material().set_emissive(emissive);
	meshes["disk"].get_material().set_specular(specular);
	meshes["disk"].get_material().set_shininess(shininess);
	// Magenta cylinder
	meshes["cylinder"].get_material().set_diffuse(vec4(1.0f, 0.0f, 1.0f, 1.0f));
	meshes["cylinder"].get_material().set_emissive(emissive);
	meshes["cylinder"].get_material().set_specular(specular);
	meshes["cylinder"].get_material().set_shininess(shininess);
	// Cyan sphere
	meshes["sphere"].get_material().set_diffuse(vec4(0.0f, 1.0f, 1.0f, 1.0f));
	meshes["sphere"].get_material().set_emissive(emissive);
	meshes["sphere"].get_material().set_specular(specular);
	meshes["sphere"].get_material().set_shininess(shininess);
	// White torus
	meshes["torus"].get_material().set_diffuse(vec4(1.0f, 1.0f, 1.0f, 1.0f));
	meshes["torus"].get_material().set_emissive(emissive);
	meshes["torus"].get_material().set_specular(specular);
	meshes["torus"].get_material().set_shininess(shininess);
    

    // ************
    // Load texture
    // ************
	tex = texture("../resources/textures/checked.gif");

	// *******************
	// Set lighting values
	// *******************
	// Point 0
	// Position (-25, 5, -15)
	points[0].set_position(vec3(-25.0f, 5.0f, -15.0f));

	// Red
	points[0].set_light_colour(vec4(1.0f, 0.0f, 0.0f, 1.0f));

	// 20 range
	points[0].set_range(20.0f);

	// Point 1
	// Position (-25, 5, -35)
	points[1].set_position(vec3(-25.0f, 5.0f, -35.0f));

	// Red
	points[1].set_light_colour(vec4(1.0f, 0.0f, 0.0f, 1.0f));

	// 20 range
	points[1].set_range(20.0f);

	// Point 2
	// Position (-10, 5, -15)
	points[2].set_position(vec3(-10.0f, 5.0f, -15.0f));

	// Red
	points[2].set_light_colour(vec4(1.0f, 0.0f, 0.0f, 1.0f));

	// 20 range
	points[2].set_range(20.0f);


	// Point 3
	// Position (-10, 5, -35)
	points[3].set_position(vec3(-10.0f, 5.0f, -35.0f));
	// Red
	points[3].set_light_colour(vec4(1.0f, 0.0f, 0.0f, 1.0f));
	// 20 range
	points[3].set_range(20.0f);

	// Spot 0
	// Position (-25, 10, -15)
	spots[0].set_position(vec3(-25.0f, 10.0f, -15.0f));
	// Green
	spots[0].set_light_colour(vec4(0.0f, 1.0f, 0.0f, 1.0f));
	// Direction (1, -1, -1) normalized
	spots[0].set_direction(vec3(1.0f, -1.0f, -1.0f));
	// 20 range
	spots[0].set_range(20.0f);
	// 0.5 power
	spots[0].set_power(0.5f);

	// Spot 1
	// Position (-25, 10, -35)
	spots[1].set_position(vec3(-25.0f, 10.0f, -35.0f));
	// Green
	spots[1].set_light_colour(vec4(0.0f, 1.0f, 0.0f, 1.0f));
	// Direction (1, -1, 1) normalized
	spots[1].set_direction(vec3(1.0f, -1.0f, 1.0f));
	// 20 range
	spots[1].set_range(20.0f);
	// 0.5 power
	spots[1].set_power(0.5f);

	// Spot 2
	// Position (-10, 10, -15)
	spots[2].set_position(vec3(-10.0f, 10.0f, -15.0f));
	// Green
	spots[2].set_light_colour(vec4(0.0f, 1.0f, 0.0f, 1.0f));
	// Direction (-1, -1, -1) normalized
	spots[2].set_direction(vec3(-1.0f, -1.0f, -1.0f));
	// 20 range
	spots[2].set_range(20.0f);
	// 0.5 power
	spots[2].set_range(0.5f);

	// Spot 3
	// Position (-10, 10, -35)
	spots[3].set_position(vec3(-10.0f, 10.0f, -35.0f));
	// Green
	spots[3].set_light_colour(vec4(0.0f, 1.0f, 0.0f, 1.0f));
	// Direction (-1, -1, 1) normalized
	spots[3].set_direction(vec3(-1.0f, -1.0f, 1.0f));
	// 20 range
	spots[3].set_range(20.0f);
	// 0.5 power
	spots[3].set_range(0.5f);

	// Spot 4
	// Position (-17.5, 15, -25)
	spots[4].set_position(vec3(-17.5f, 15.0f, -25.0f));
	// Blue
	spots[4].set_light_colour(vec4(0.0f, 0.0f, 1.0f, 1.0f));
	// Direction (0, -1, 0)
	spots[4].set_direction(vec3(0.0f, -1.0f, 0.0f));
	// 30 range
	spots[4].set_range(30.0f);
	// 1.0 power
	spots[4].set_range(1.0f);

	// effect for phong
	phong_eff.add_shader("shader.vert", GL_VERTEX_SHADER); 

	vector<string> frag_shaders
	{
		"shader.frag",
		"..\\resources\\shaders\\parts\\direction.frag",
		"..\\resources\\shaders\\parts\\point.frag",
		"..\\resources\\shaders\\parts\\spot.frag"
	};
	phong_eff.add_shader(frag_shaders, GL_FRAGMENT_SHADER);

	phong_eff.build();

	// effect for gouraud
	gouraud_eff.add_shader("..\\resources\\shaders\\multi-file-gouraud\\shader.frag", GL_FRAGMENT_SHADER);
	
	vector<string> vert_shaders
	{
		"..\\resources\\shaders\\multi-file-gouraud\\shader.vert",
		"..\\resources\\shaders\\multi-file-gouraud\\direction.vert",
		"..\\resources\\shaders\\multi-file-gouraud\\point_g.vert",
		"..\\resources\\shaders\\multi-file-gouraud\\spot_g.vert"
	};
	gouraud_eff.add_shader(vert_shaders, GL_VERTEX_SHADER);

	gouraud_eff.build();

   

    // Set camera properties
    cam.set_position(vec3(50.0f, 10.0f, 50.0f));
    cam.set_target(vec3(0.0f, 0.0f, 0.0f));
    auto aspect = static_cast<float>(renderer::get_screen_width()) / static_cast<float>(renderer::get_screen_height());
    cam.set_projection(quarter_pi<float>(), aspect, 2.414f, 1000.0f);
    return true;
}

bool update(float delta_time)
{
    if (glfwGetKey(renderer::get_window(), '1'))
        cam.set_position(vec3(50, 10, 50));
    if (glfwGetKey(renderer::get_window(), '2'))
        cam.set_position(vec3(-50, 10, 50));
    if (glfwGetKey(renderer::get_window(), '3'))
        cam.set_position(vec3(-50, 10, -50));
    if (glfwGetKey(renderer::get_window(), '4'))
        cam.set_position(vec3(50, 10, -50));
	if (glfwGetKey(renderer::get_window(), GLFW_KEY_G))
		lightingType = "gouraud";
	if (glfwGetKey(renderer::get_window(), GLFW_KEY_P))
		lightingType = "phong";

    // Rotate the sphere
    meshes["sphere"].get_transform().rotate(vec3(0.0f, half_pi<float>(), 0.0f) * delta_time);

    cam.update(delta_time);

    return true;
}

bool render()
{
    // Render meshes
    for (auto &e : meshes)
    {
		if (lightingType == "phong")
		{
			auto m = e.second;
			// Bind effect
			renderer::bind(phong_eff);
			// Create MVP matrix
			auto M = m.get_transform().get_transform_matrix();
			auto V = cam.get_view();
			auto P = cam.get_projection();
			auto MVP = P * V * M;
			// Set MVP matrix uniform
			glUniformMatrix4fv(
				phong_eff.get_uniform_location("MVP"), // Location of uniform
				1, // Number of values - 1 mat4
				GL_FALSE, // Transpose the matrix?
				value_ptr(MVP)); // Pointer to matrix data
			// ********************
			// Set M matrix uniform
			// ********************
			glUniformMatrix4fv(
				phong_eff.get_uniform_location("M"), // Location of uniform
				1, // Number of values - 1 mat4
				GL_FALSE, // Transpose the matrix?
				value_ptr(M)); // Pointer to matrix data

			// ***********************
			// Set N matrix uniform
			// - remember - 3x3 matrix
			// ***********************
			auto N = m.get_transform().get_normal_matrix();

			glUniformMatrix3fv(
				phong_eff.get_uniform_location("N"), // Location of uniform
				1, // Number of values - 1 mat4
				GL_FALSE, // Transpose the matrix?
				value_ptr(N)); // Pointer to normal matrix data

			// *************
			// Bind material
			// *************
			renderer::bind(m.get_material(), "mat");
			// **********************
			// Bind directional light
			// **********************
			renderer::bind(light, "light");
			// *****************
			// Bind point lights
			// *****************
			renderer::bind(points, "points");
			// ****************
			// Bind spot lights
			// ****************
			renderer::bind(spots, "spots");
			// ************
			// Bind texture
			// ************
			renderer::bind(tex, 0);
			// ***************
			// Set tex uniform
			// ***************
			glUniform1i(phong_eff.get_uniform_location("tex"), 0);
			// *****************************
			// Set eye position
			// - Get this from active camera
			// *****************************
			glUniform3fv(phong_eff.get_uniform_location("eye_pos"), 1, value_ptr(cam.get_position()));

			// Render mesh
			renderer::render(m);
		}
		else
		{
			auto m = e.second;
			// Bind effect
			renderer::bind(gouraud_eff);
			// Create MVP matrix
			auto M = m.get_transform().get_transform_matrix();
			auto V = cam.get_view();
			auto P = cam.get_projection();
			auto MVP = P * V * M;
			// Set MVP matrix uniform
			glUniformMatrix4fv(
				gouraud_eff.get_uniform_location("MVP"), // Location of uniform
				1, // Number of values - 1 mat4
				GL_FALSE, // Transpose the matrix?
				value_ptr(MVP)); // Pointer to matrix data
			// ********************
			// Set M matrix uniform
			// ********************
			glUniformMatrix4fv(
				gouraud_eff.get_uniform_location("M"), // Location of uniform
				1, // Number of values - 1 mat4
				GL_FALSE, // Transpose the matrix?
				value_ptr(M)); // Pointer to matrix data

			// ***********************
			// Set N matrix uniform
			// - remember - 3x3 matrix
			// ***********************
			auto N = m.get_transform().get_normal_matrix();

			glUniformMatrix3fv(
				gouraud_eff.get_uniform_location("N"), // Location of uniform
				1, // Number of values - 1 mat4
				GL_FALSE, // Transpose the matrix?
				value_ptr(N)); // Pointer to normal matrix data

			// *************
			// Bind material
			// *************
			renderer::bind(m.get_material(), "mat");
			// **********************
			// Bind directional light
			// **********************
			renderer::bind(light, "light");
			// *****************
			// Bind point lights
			// *****************
			renderer::bind(points, "points");
			// ****************
			// Bind spot lights
			// ****************
			renderer::bind(spots, "spots");
			// ************
			// Bind texture
			// ************
			renderer::bind(tex, 0);
			// ***************
			// Set tex uniform
			// ***************
			glUniform1i(gouraud_eff.get_uniform_location("tex"), 0);
			// *****************************
			// Set eye position
			// - Get this from active camera
			// *****************************
			glUniform3fv(gouraud_eff.get_uniform_location("eye_pos"), 1, value_ptr(cam.get_position()));

			// Render mesh
			renderer::render(m);
		}
    }

    return true;
}

void main()
{
    // Create application
    app application;
    // Set load content, update and render methods
    application.set_load_content(load_content);
    application.set_update(update);
    application.set_render(render);
    // Run application
    application.run();
}