#include <graphics_framework.h>
#include <glm/glm.hpp>

using namespace std;
using namespace graphics_framework;
using namespace glm;

mesh terr;
effect eff;
effect grass_eff;
//target_camera cam;
directional_light light;
texture tex[4];

free_camera cam;
texture grassDissolve;
texture grassTex;
//vec3 pos = m.get_transform().position;

geometry grassGeom;
// Initial mouse input
double cursor_x = 0.0;
double cursor_y = 0.0;

// Contains terrain position data
vector<vec3> positions;

// Contains grass position data
vector<vec3> grassPositions;

// Dissolve factor to set on shader
float dissolve_factor = 0.4f;

bool initialise()
{
	// ********************************
	// Set input mode - hide the cursor
	// ********************************
	glfwSetInputMode(renderer::get_window(), GLFW_CURSOR, GLFW_CURSOR_DISABLED);

	// ******************************
	// Capture initial mouse position
	// ******************************
	glfwGetCursorPos(renderer::get_window(), &cursor_x, &cursor_y);

	return true;
}
void generate_terrain(geometry &geom, const texture &height_map, unsigned int width, unsigned int depth, float height_scale)
{
    // Contains our normal data
    vector<vec3> normals;
    // Contains our texture coordinate data
    vector<vec2> tex_coords;
    // Contains our texture weights
    vector<vec4> tex_weights;
    // Contains our index data
    vector<unsigned int> indices;

    // ***************************************
    // Extract the texture data from the image
    // ***************************************
    glBindTexture(GL_TEXTURE_2D, height_map.get_id());
    auto data = new vec4[height_map.get_width() * height_map.get_height()];
    glGetTexImage(GL_TEXTURE_2D, 0, GL_RGBA, GL_FLOAT, (void*)data);

    // Determine ratio of height map to geometry
    float width_point = static_cast<float>(width) / static_cast<float>(height_map.get_width());
    float depth_point = static_cast<float>(depth) / static_cast<float>(height_map.get_height());

    // Point to work on
    vec3 point;

    // ***********************************************************************
    // Part 1 - Iterate through each point, calculate vertex and add to vector
    // ***********************************************************************
    for (int x = 0; x < height_map.get_width(); ++x)
    {
        // *****************************
        // Calculate x position of point
        // *****************************
        point.x = -(width / 2.0f) + (width_point * static_cast<float>(x));
        
        for (int z = 0; z < height_map.get_height(); ++z)
        {
            // *****************************
            // Calculate z position of point
            // *****************************
            point.z = -(depth / 2.0f) + (depth_point * static_cast<float>(z));
            // ****************************************************
            // Y position based on red component of height map data
            // ****************************************************
            point.y = data[(z * height_map.get_width()) + x].y * height_scale;
            // **************************
            // Add point to position data
            // **************************
            positions.push_back(point);
        }
    }

    // ***********************
    // Part 1 - Add index data
    // ***********************
    for (unsigned int x = 0; x < height_map.get_width() - 1; ++x)
    {
        for (unsigned int y = 0; y < height_map.get_height() - 1; ++y)
        {
            // *************************
            // Get four corners of patch
            // *************************
            unsigned int top_left = (y * height_map.get_width()) + x;
            unsigned int top_right = (y * height_map.get_width()) + x + 1;
            unsigned int bottom_left = ((y + 1) * height_map.get_width()) + x;
            unsigned int bottom_right = ((y + 1) * height_map.get_width()) + x + 1;
            // ********************************
            // Push back indices for triangle 1
            // ********************************
            indices.push_back(top_left);
            indices.push_back(bottom_right);
            indices.push_back(bottom_left);
            // ********************************
            // Push back indices for triangle 2
            // ********************************
            indices.push_back(top_left);
            indices.push_back(top_right);
            indices.push_back(bottom_right);
        }
    }

    // Resize the normals buffer
    normals.resize(positions.size());

    // *********************************************
    // Part 2 - Calculate normals for the height map
    // *********************************************
    for (unsigned int i = 0; i < indices.size() / 3; ++i)
    {
        // ****************************
        // Get indices for the triangle
        // ****************************
        auto idx1 = indices[i * 3];
        auto idx2 = indices[i * 3 + 1];
        auto idx3 = indices[i * 3 + 2];

        // ***********************************
        // Calculate two sides of the triangle
        // ***********************************
        vec3 side1 = positions[idx1] - positions[idx3];
        vec3 side2 = positions[idx1] - positions[idx2];

        // ******************************************
        // Normal is cross product of these two sides
        // ******************************************
        vec3 normal = normalize(cross(side2, side1));

        // **********************************************************************
        // Add to normals in the normal buffer using the indices for the triangle
        // **********************************************************************
        normals[idx1] += normal;
        normals[idx2] += normal;
        normals[idx3] += normal;
    }

    // *************************
    // Part 2 - Normalize all the normals
    // *************************
    for (auto &n : normals)
        n = normalize(n);

    // *********************************************
    // Part 3 - Add texture coordinates for geometry
    // *********************************************
    for (unsigned int x = 0; x < height_map.get_width(); ++x)
    {
        for (unsigned int z = 0; z < height_map.get_height(); ++z)
        {
            tex_coords.push_back(vec2(width_point * x, depth_point * z));
        }
    }

    // **************************************************
    // Part 4 - Calculate texture weights for each vertex
    // **************************************************
    for (unsigned int x = 0; x < height_map.get_width(); ++x)
    {
        for (unsigned int z = 0; z < height_map.get_height(); ++z)
        {
            // ********************
            // Calculate tex weight
            // ********************
            vec4 tex_weight(
                clamp(1.0f - abs(data[(height_map.get_width() * z) + x].y - 0.0f) / 0.25f, 0.0f, 1.0f),
                clamp(1.0f - abs(data[(height_map.get_width() * z) + x].y - 0.15f) / 0.25f, 0.0f, 1.0f),
                clamp(1.0f - abs(data[(height_map.get_width() * z) + x].y - 0.5f) / 0.25f, 0.0f, 1.0f),
                clamp(1.0f - abs(data[(height_map.get_width() * z) + x].y - 0.9f) / 0.25f, 0.0f, 1.0f));

            // ********************************
            // Sum the components of the vector
            // ********************************
            auto total = tex_weight.x + tex_weight.y + tex_weight.z + tex_weight.w;
            
            // ********************
            // Divide weight by sum
            // ********************
            tex_weight /= total;

            // *************************
            // Add tex weight to weights
            // *************************
            tex_weights.push_back(tex_weight);
        }
    }

    // *************************************
    // Add necessary buffers to the geometry
    // *************************************
    geom.add_buffer(positions, BUFFER_INDEXES::POSITION_BUFFER);
    geom.add_buffer(normals, BUFFER_INDEXES::NORMAL_BUFFER);
    geom.add_buffer(tex_coords, BUFFER_INDEXES::TEXTURE_COORDS_0);
    geom.add_buffer(tex_weights, BUFFER_INDEXES::TEXTURE_COORDS_1);
    geom.add_index_buffer(indices);

    // ***********
    // Delete data
    // ***********
    delete[] data;
}

void generate_grass()
{
	default_random_engine e;
	uniform_int_distribution<int> dist(0, 1000);

	for each (vec3 pos in positions)
	{

		if (dist(e) >= 995 && pos.y < 2.0f)
		{
			grassPositions.push_back(vec3(pos.x, pos.y + 0.5f, pos.z));

		}

	}
	// Create geometry using these points
	grassGeom.add_buffer(grassPositions, BUFFER_INDEXES::POSITION_BUFFER);
	// Set geometry type to points
	grassGeom.set_type(GL_POINTS);
}

bool load_content()
{
    // Geometry to load into
    geometry geom;

    // Load height map
    texture height_map("..\\resources\\textures\\heightmaps\\terrain.png");

    // Generate terrain
    generate_terrain(geom, height_map, 40, 40, 6.0f);

    // Use geometry to create terrain mesh
    terr = mesh(geom);

    // ************************
    // Load in necessary effect
    // - Part 1 colour
    // - Part 2 lighting
    // - Part 3 textured
    // - Part 4 texture weighted
    // *************************
	eff.add_shader("terrain.vert", GL_VERTEX_SHADER);
	vector<string> frag_shaders
	{
		"terrain.frag",
		"..\\resources\\shaders\\parts\\direction.frag",
		"..\\resources\\shaders\\parts\\weighted_texture.frag",
	};
	eff.add_shader(frag_shaders, GL_FRAGMENT_SHADER);
    
    // Build effect
    eff.build();

	generate_grass();

	// Texture we use to texture the billboards
	grassTex = texture("..\\resources\\textures\\low_poly_grass2.png");
	grassDissolve = texture("../resources/textures/low_poly_grass_opacity.png");

	// Load shader
	grass_eff.add_shader("..\\resources\\shaders\\grass_shader.vert", GL_VERTEX_SHADER);
	grass_eff.add_shader("..\\resources\\shaders\\billboard.geom", GL_GEOMETRY_SHADER);
	grass_eff.add_shader("..\\resources\\shaders\\grass_shader.frag", GL_FRAGMENT_SHADER);
	grass_eff.build();


    // ?????????? remove
    light.set_ambient_intensity(vec4(0.3f, 0.3f, 0.3f, 1.0f));
    light.set_light_colour(vec4(0.9f, 0.9f, 0.9f, 1.0f));
    light.set_direction(normalize(vec3(1.0f, 1.0f, 1.0f)));
    terr.get_material().set_diffuse(vec4(0.5f, 0.5f, 0.5f, 1.0f));
    terr.get_material().set_specular(vec4(0.0f, 0.0f, 0.0f, 1.0f));
    terr.get_material().set_shininess(20.0f);
    terr.get_material().set_emissive(vec4(0.0f, 0.0f, 0.0f, 1.0f));
    tex[0] = texture("..\\resources\\textures\\sand.dds");
    tex[1] = texture("..\\resources\\textures\\grass.dds");
    tex[2] = texture("..\\resources\\textures\\rock.dds");
    tex[3] = texture("..\\resources\\textures\\snow.dds");

    // Set camera properties
    cam.set_position(vec3(0.0f, 5.0f, 10.0f));
    cam.set_target(vec3(0.0f, 0.0f, 0.0f));
    auto aspect = static_cast<float>(renderer::get_screen_width()) / static_cast<float>(renderer::get_screen_height());
    cam.set_projection(quarter_pi<float>(), aspect, 2.414f, 1000.0f);
    return true;
}

bool update(float delta_time)
{
	// The ratio of pixels to rotation - remember the fov
	static double ratio_width = quarter_pi<float>() / static_cast<float>(renderer::get_screen_width());
	static double ratio_height = (quarter_pi<float>() * (static_cast<float>(renderer::get_screen_height()) / static_cast<float>(renderer::get_screen_width()))) / static_cast<float>(renderer::get_screen_height());

	double current_x;
	double current_y;
	// *******************************
	// Get the current cursor position
	// *******************************
	glfwGetCursorPos(renderer::get_window(), &current_x, &current_y);


	// ***************************************************
	// Calculate delta of cursor positions from last frame
	// ***************************************************
	double delta_x = current_x - cursor_x;
	double delta_y = current_y - cursor_y;

	// *************************************************************
	// Multiply deltas by ratios - gets actual change in orientation
	// *************************************************************
	delta_x = delta_x * ratio_width;
	delta_y = delta_y * ratio_height;

	// *************************
	// Rotate cameras by delta
	// delta_y - x-axis rotation
	// delta_x - y-axis rotation
	// *************************
	cam.rotate(delta_x, -delta_y); // -delta_y, otherwise up and down is reversed

	// *******************************
	// Use keyboard to move the camera
	// - WSAD
	// *******************************

	const float speed = 1.0f; // Speed of camera movement

	vec3 translation(0.0f, 0.0f, 0.0f);

	// Check if key is pressed
	if (glfwGetKey(renderer::get_window(), GLFW_KEY_W))
		cam.move(vec3(0.0f, 0.0f, speed)); // translation = vec3(0.0f, 0.0f, 1.0f); (If you do it this way then you cant have 2 translations at the same time)
	if (glfwGetKey(renderer::get_window(), GLFW_KEY_S))
		cam.move(vec3(0.0f, 0.0f, -speed)); // translation = vec3(0.0f, 0.0f, -1.0f);
	if (glfwGetKey(renderer::get_window(), GLFW_KEY_A))
		cam.move(vec3(-speed, 0.0f, 0.0f)); // translation = vec3(-1.0f, 0.0f, 0.0f);
	if (glfwGetKey(renderer::get_window(), GLFW_KEY_D))
		cam.move(vec3(speed, 0.0f, 0.0f)); // translation = vec3(1.0f, 0.0f, 0.0f);

	// ***********
	// Move camera
	// ***********

	//cam.move(translation);

	// *****************
	// Update the camera
	// *****************
	cam.update(delta_time);


	// *****************
	// Update cursor pos
	// *****************
	cursor_x = current_x;
	cursor_y = current_y;

	// Use up an down to modify the dissolve factor
	if (glfwGetKey(renderer::get_window(), GLFW_KEY_LEFT))
	{
		dissolve_factor = clamp(dissolve_factor + 0.5f * delta_time, 0.0f, 1.0f);
		cout << dissolve_factor << endl;
	}
	if (glfwGetKey(renderer::get_window(), GLFW_KEY_RIGHT))
	{
		dissolve_factor = clamp(dissolve_factor - 0.5f * delta_time, 0.0f, 1.0f);
		cout << dissolve_factor << endl;
	}

    return true;
}

void renderGrass()
{
	// Simply render the points.  All the work done in the geometry shader
	renderer::bind(grass_eff);
	auto V = cam.get_view();
	auto P = cam.get_projection();
	auto MVP = P * V;
	glUniformMatrix4fv(
		grass_eff.get_uniform_location("MV"),
		1,
		GL_FALSE,
		value_ptr(V));
	glUniformMatrix4fv(
		grass_eff.get_uniform_location("P"),
		1,
		GL_FALSE,
		value_ptr(P));
	glUniform1f(grass_eff.get_uniform_location("point_size"), 2.0f);
	renderer::bind(grassTex, 0);
	renderer::bind(grassDissolve, 1);
	glUniform1i(grass_eff.get_uniform_location("tex"), 0);
	glUniform1i(grass_eff.get_uniform_location("dissolve"), 1);

	glUniform1f(grass_eff.get_uniform_location("dissolve_factor"), dissolve_factor);

	renderer::render(grassGeom);
}

bool render()
{
    // Bind effect
    renderer::bind(eff);
    // Create MVP matrix
    auto M = terr.get_transform().get_transform_matrix();
    auto V = cam.get_view();
    auto P = cam.get_projection();
    auto MVP = P * V * M;
    // Set MVP matrix uniform
    glUniformMatrix4fv(
        eff.get_uniform_location("MVP"),
        1,
        GL_FALSE,
        value_ptr(MVP));
    
    glUniform4fv(eff.get_uniform_location("colour"), 1, value_ptr(vec4(0.7f, 0.7f, 0.7f, 1.0f)));
    // ****************************
    // Set other necessary uniforms
    // ****************************

    // ?????????? change these to colour
    glUniformMatrix4fv(
        eff.get_uniform_location("M"),
        1,
        GL_FALSE,
        value_ptr(M));
    glUniformMatrix3fv(
        eff.get_uniform_location("N"),
        1,
        GL_FALSE,
        value_ptr(terr.get_transform().get_normal_matrix()));
    renderer::bind(terr.get_material(), "mat");
    renderer::bind(light, "light");
    renderer::bind(tex[0], 0);
    glUniform1i(eff.get_uniform_location("tex[0]"), 0);
    renderer::bind(tex[1], 1);
    glUniform1i(eff.get_uniform_location("tex[1]"), 1);
    renderer::bind(tex[2], 2);
    glUniform1i(eff.get_uniform_location("tex[2]"), 2);
    renderer::bind(tex[3], 3);
    glUniform1i(eff.get_uniform_location("tex[3]"), 3);
    glUniform3fv(eff.get_uniform_location("eye_pos"), 1, value_ptr(cam.get_position()));

    // Render terrain
    renderer::render(terr);

	renderGrass();


    return true;
}

void main()
{
    // Create application
    app application;
    // Set methods
    application.set_load_content(load_content);
	application.set_initialise(initialise);
    application.set_update(update);
    application.set_render(render);
    // Run application
    application.run();
}