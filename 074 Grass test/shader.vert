#version 440

// Vertex position
layout (location = 0) in vec3 position;

layout (location = 10) in vec2 tex_coord_in;

layout (location = 0) out vec2 tex_coord_out;

void main()
{
	// Pass through position to geometry shader
	gl_Position = vec4(position, 1.0);
	// Pass through texture coordinate
	tex_coord_out = tex_coord_in;
}