#version 440

uniform sampler2D tex;
uniform sampler2D dissolve;

//Dissolve factor value
uniform float dissolve_factor;

// Incoming texture coordinates
layout (location = 0) in vec2 tex_coord;

// Incoming vertex colour
//layout (location = 0) in vec4 in_colour;

// Outgoing pixel colour
layout (location = 0) out vec4 out_colour;

void main()
{
	// Get dissolve value from the dissolve texture
	vec4 dissolve_value = texture(dissolve, tex_coord);

	// If b component is greater than dissolve factor, discard
	if (dissolve_value.b > dissolve_factor)
		discard;

	// Get texture colour
	out_colour = texture(tex, tex_coord);
}