#include <graphics_framework.h>
#include <glm\glm.hpp>

using namespace std;
using namespace graphics_framework;
using namespace glm;

geometry geom;
effect eff;
//target_camera cam;
mesh m;
// Main texture
texture tex;
// Dissolve texture
texture dissolve;

// Dissolve factor to set on shader
float dissolve_factor = 1.0f;

free_camera cam;

// Initial mouse input
double cursor_x = 0.0;
double cursor_y = 0.0;

bool initialise()
{
	// ********************************
	// Set input mode - hide the cursor
	// ********************************
	glfwSetInputMode(renderer::get_window(), GLFW_CURSOR, GLFW_CURSOR_DISABLED);

	// ******************************
	// Capture initial mouse position
	// ******************************
	glfwGetCursorPos(renderer::get_window(), &cursor_x, &cursor_y);

	return true;
}

bool load_content()
{
    geom.set_type(GL_QUADS);
	vector<vec3> positions
	{
		// Front
		vec3(1.0f, 1.0f, 1.0f),
		vec3(-1.0f, 1.0f, 1.0f),
		vec3(-1.0f, -1.0f, 1.0f),
		vec3(1.0f, -1.0f, 1.0f),
		// Right
		vec3(-0.5f, 1.0f, 1.5f),
		vec3(0.5f, 1.0f, 0.5f),
		vec3(0.5f, -1.0f, 0.5f),
		vec3(-0.5f, -1.0f, 1.5f),
		// Left
		vec3(0.0f, 1.0f, 0.0f),
		vec3(0.0f, 1.0f, 2.0f),
		vec3(0.0f, -1.0f, 2.0f),
		vec3(0.0f, -1.0f, 0.0f),
	};
	// Texture coordinates
	vector<vec2> tex_coords;
	// Six sides, 6 vertices per side
	for (unsigned int i = 0; i < 3; ++i)
	{
		tex_coords.push_back(vec2(1.0f, 1.0f));
		tex_coords.push_back(vec2(0.0f, 1.0f));
		tex_coords.push_back(vec2(0.0f, 0.0f));
		tex_coords.push_back(vec2(1.0f, 0.0f));
	}
	// Add to the geometry
	geom.add_buffer(positions, BUFFER_INDEXES::POSITION_BUFFER);
	geom.add_buffer(tex_coords, BUFFER_INDEXES::TEXTURE_COORDS_0);
    //geom.add_buffer(colours, BUFFER_INDEXES::COLOUR_BUFFER);
	m = mesh(geom);
	// Scale geometry
	m.get_transform().scale = vec3(10.0f, 10.0f, 10.0f);


    // Load in shaders
    eff.add_shader("shader.vert", GL_VERTEX_SHADER);
    eff.add_shader("shader.frag", GL_FRAGMENT_SHADER);

    // ********************
    // Load geometry shader
    // ********************
	eff.add_shader("shader.geom", GL_GEOMETRY_SHADER);

    // Build effect
    eff.build();

	// Load in textures
	tex = texture("../resources/textures/low_poly_grass2.png");
	dissolve = texture("../resources/textures/low_poly_grass_opacity.png");

    // Set camera properties
    cam.set_position(vec3(80.0f, 80.0f, 80.0f));
    cam.set_target(vec3(0.0f, 0.0f, 0.0f));
    auto aspect = static_cast<float>(renderer::get_screen_width()) / static_cast<float>(renderer::get_screen_height());
    cam.set_projection(quarter_pi<float>(), aspect, 2.414f, 1000.0f);
    return true;
}

bool update(float delta_time)
{
	
	// The ratio of pixels to rotation - remember the fov
	static double ratio_width = quarter_pi<float>() / static_cast<float>(renderer::get_screen_width());
	static double ratio_height = (quarter_pi<float>() * (static_cast<float>(renderer::get_screen_height()) / static_cast<float>(renderer::get_screen_width()))) / static_cast<float>(renderer::get_screen_height());

	double current_x;
	double current_y;
	// *******************************
	// Get the current cursor position
	// *******************************
	glfwGetCursorPos(renderer::get_window(), &current_x, &current_y);


	// ***************************************************
	// Calculate delta of cursor positions from last frame
	// ***************************************************
	double delta_x = current_x - cursor_x;
	double delta_y = current_y - cursor_y;

	// *************************************************************
	// Multiply deltas by ratios - gets actual change in orientation
	// *************************************************************
	delta_x = delta_x * ratio_width;
	delta_y = delta_y * ratio_height;

	// *************************
	// Rotate cameras by delta
	// delta_y - x-axis rotation
	// delta_x - y-axis rotation
	// *************************
	cam.rotate(delta_x, -delta_y); // -delta_y, otherwise up and down is reversed

	// *******************************
	// Use keyboard to move the camera
	// - WSAD
	// *******************************

	const float speed = 1.0f; // Speed of camera movement

	vec3 translation(0.0f, 0.0f, 0.0f);

	// Check if key is pressed
	if (glfwGetKey(renderer::get_window(), GLFW_KEY_W))
		cam.move(vec3(0.0f, 0.0f, speed)); // translation = vec3(0.0f, 0.0f, 1.0f); (If you do it this way then you cant have 2 translations at the same time)
	if (glfwGetKey(renderer::get_window(), GLFW_KEY_S))
		cam.move(vec3(0.0f, 0.0f, -speed)); // translation = vec3(0.0f, 0.0f, -1.0f);
	if (glfwGetKey(renderer::get_window(), GLFW_KEY_A))
		cam.move(vec3(-speed, 0.0f, 0.0f)); // translation = vec3(-1.0f, 0.0f, 0.0f);
	if (glfwGetKey(renderer::get_window(), GLFW_KEY_D))
		cam.move(vec3(speed, 0.0f, 0.0f)); // translation = vec3(1.0f, 0.0f, 0.0f);

	// Update the camera
	cam.update(delta_time);

	// *****************
	// Update cursor pos
	// *****************
	cursor_x = current_x;
	cursor_y = current_y;

	// Use up an down to modify the dissolve factor
	if (glfwGetKey(renderer::get_window(), GLFW_KEY_UP))
	{
		dissolve_factor = clamp(dissolve_factor + 0.5f * delta_time, 0.0f, 1.0f);
		cout << dissolve_factor << endl;
	}
	if (glfwGetKey(renderer::get_window(), GLFW_KEY_DOWN))
	{
		dissolve_factor = clamp(dissolve_factor - 0.5f * delta_time, 0.0f, 1.0f);
		cout << dissolve_factor << endl;
	}

    return true;
}

bool render()
{
	vec3 offset = vec3(1.0f, 0.0f, 0.0f);

	// Bind effect
	renderer::bind(eff);
	// Create MVP matrix
	mat4 M = m.get_transform().get_transform_matrix();//M(1.0f);
	auto V = cam.get_view();
	auto P = cam.get_projection();
	auto MVP = P * V * M;
	glDisable(GL_CULL_FACE);
	// Set MVP matrix uniform
	glUniformMatrix4fv(
	    eff.get_uniform_location("MVP"), // Location of uniform
	    1, // Number of values - 1 mat4
	    GL_FALSE, // Transpose the matrix?
	    value_ptr(MVP)); // Pointer to matrix data


	// *************************************
	// Set the dissolve_factor uniform value
	// *************************************
	glUniform1f(eff.get_uniform_location("dissolve_factor"), dissolve_factor);

	// ****************************************************
	// Bind the two textures - use different index for each
	// ****************************************************
	renderer::bind(tex, 0);
	renderer::bind(dissolve, 1);

	// *******************************************************
	// Set the uniform values for textures - use correct index
	// *******************************************************
	glUniform1i(eff.get_uniform_location("tex"), 0);
	glUniform1i(eff.get_uniform_location("dissolve"), 1);


	// **********
	// Set offset
	// **********
	glUniform3fv(eff.get_uniform_location("offset"), 1, value_ptr(offset));

	cout << cam.get_position().x << cam.get_position().y << cam.get_position().z << endl;

	// Render geometry
	renderer::render(m);

	return true;
}

void main()
{
    // Create application
    app application;
    // Set load content, update and render methods
    application.set_load_content(load_content);
	application.set_initialise(initialise);
    application.set_update(update);
    application.set_render(render);
    // Run application
    application.run();
}