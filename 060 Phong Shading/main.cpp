#include "Node.h"
#include <graphics_framework.h>
#include <glm\glm.hpp>

using namespace std;
using namespace graphics_framework;
using namespace glm;

map<string, mesh> meshes;
effect eff;
texture tex;
target_camera cam;
directional_light light;
material mat;
Node root;
Node child1;
Node child2;
float theta = 20.0f;
float rho = 0.0f;

vec3 xrot(1.0f, 0.0f, 0.0f);
vec3 yrot(0.0f, 1.0f, 0.0f);

bool load_content()
{
	cout << "for root" << endl;
	root.add_child(&child1);
	root.print_number_of_children();
	child1.add_child(&child2);
	root.print_number_of_children();

	// Create plane mesh
	meshes["plane"] = mesh(geometry_builder::create_plane());
	// Create scene
	meshes["box"] = mesh(geometry_builder::create_box());
	meshes["tetra"] = mesh(geometry_builder::create_tetrahedron());
	//meshes["pyramid"] = mesh(geometry_builder::create_pyramid());
	//meshes["disk"] = mesh(geometry_builder::create_disk(20));
	//meshes["cylinder"] = mesh(geometry_builder::create_cylinder(20, 20));
	//meshes["sphere"] = mesh(geometry_builder::create_sphere(20, 20));
	//meshes["torus"] = mesh(geometry_builder::create_torus(20, 20, 1.0f, 5.0f));

	// Transform objects
	meshes["box"].get_transform().scale = vec3(5.0f, 5.0f, 5.0f);
	meshes["box"].get_transform().translate(vec3(50.0f, 2.5f, 0.0f));
	meshes["tetra"].get_transform().scale = vec3(2.0f, 2.0f, 2.0f);
	meshes["tetra"].get_transform().translate(vec3(5.0f, 2.5f, 0.0f));
	//meshes["pyramid"].get_transform().scale = vec3(5.0f, 5.0f, 5.0f);
	//meshes["pyramid"].get_transform().translate(vec3(-10.0f, 7.5f, -30.0f));
	//meshes["disk"].get_transform().scale = vec3(3.0f, 1.0f, 3.0f);
	//meshes["disk"].get_transform().translate(vec3(-10.0f, 11.5f, -30.0f));
	//meshes["disk"].get_transform().rotate(vec3(half_pi<float>(), 0.0f, 0.0f));
	//meshes["cylinder"].get_transform().scale = vec3(5.0f, 5.0f, 5.0f);
	//meshes["cylinder"].get_transform().translate(vec3(-25.0f, 2.5f, -25.0f));
	//meshes["sphere"].get_transform().scale = vec3(2.5f, 2.5f, 2.5f);
	//meshes["sphere"].get_transform().translate(vec3(-25.0f, 10.0f, -25.0f));
	//meshes["torus"].get_transform().translate(vec3(-25.0f, 10.0f, -25.0f));
	//meshes["torus"].get_transform().rotate(vec3(half_pi<float>(), 0.0f, 0.0f));

	// ***********************
	// Set materials
	// - all emissive is black
	// - all specular is white
	// - all shininess is 25
	// ***********************
	const vec4 emissive(0.0f, 0.0f, 0.0f, 1.0f);
	const vec4 specular(1.0f, 1.0f, 1.0f, 1.0f);
	const float shininess = 25.0f;
	// Red box
	meshes["box"].get_material().set_diffuse(vec4(1.0f, 0.0f, 0.0f, 1.0f));
	meshes["box"].get_material().set_emissive(emissive);
	meshes["box"].get_material().set_specular(specular);
	meshes["box"].get_material().set_shininess(shininess);
	// Green tetra
	meshes["tetra"].get_material().set_diffuse(vec4(0.0f, 1.0f, 0.0f, 1.0f));
	meshes["tetra"].get_material().set_emissive(emissive);
	meshes["tetra"].get_material().set_specular(specular);
	meshes["tetra"].get_material().set_shininess(shininess);
	// Blue pyramid
	//meshes["pyramid"].get_material().set_diffuse(vec4(0.0f, 0.0f, 1.0f, 1.0f));
	//meshes["pyramid"].get_material().set_emissive(emissive);
	//meshes["pyramid"].get_material().set_specular(specular);
	//meshes["pyramid"].get_material().set_shininess(shininess);
	//// Yellow disk
	//meshes["disk"].get_material().set_diffuse(vec4(1.0f, 1.0f, 0.0f, 1.0f));
	//meshes["disk"].get_material().set_emissive(emissive);
	//meshes["disk"].get_material().set_specular(specular);
	//meshes["disk"].get_material().set_shininess(shininess);
	//// Magenta cylinder
	//meshes["cylinder"].get_material().set_diffuse(vec4(1.0f, 0.0f, 1.0f, 1.0f));
	//meshes["cylinder"].get_material().set_emissive(emissive);
	//meshes["cylinder"].get_material().set_specular(specular);
	//meshes["cylinder"].get_material().set_shininess(shininess);
	//// Cyan sphere
	//meshes["sphere"].get_material().set_diffuse(vec4(0.0f, 1.0f, 1.0f, 1.0f));
	//meshes["sphere"].get_material().set_emissive(emissive);
	//meshes["sphere"].get_material().set_specular(specular);
	//meshes["sphere"].get_material().set_shininess(shininess);
	//// White torus
	//meshes["torus"].get_material().set_diffuse(vec4(1.0f, 1.0f, 1.0f, 1.0f));
	//meshes["torus"].get_material().set_emissive(emissive);
	//meshes["torus"].get_material().set_specular(specular);
	//meshes["torus"].get_material().set_shininess(shininess);

	root.m_transform = meshes["plane"].get_transform().get_transform_matrix();
	child1.m_transform = meshes["box"].get_transform().get_transform_matrix();
	child2.m_transform = meshes["tetra"].get_transform().get_transform_matrix();
	
	root.m_mesh = meshes["plane"];
	child1.m_mesh = meshes["box"];
	child2.m_mesh = meshes["tetra"];

	// **************************
	// Load texture - checked.gif
	// **************************
	tex = texture("../resources/textures/checked.gif");
	root.m_tex = tex;
	child1.m_tex = tex;
	child2.m_tex = tex;
	// *******************
	// Set lighting values
	// *******************
	// ambient intensity (0.3, 0.3, 0.3)
	light.set_ambient_intensity(vec4(0.3f, 0.3f, 0.3f, 1.0f));
	// Light colour white
	light.set_light_colour(vec4(1.0f, 1.0f, 1.0f, 1.0f));
	// Light direction (1.0, 1.0, -1.0)
	light.set_direction(vec3(1.0f, 1.0f, -1.0f));

	// Load in shaders
	eff.add_shader("..\\resources\\shaders\\phong.vert", GL_VERTEX_SHADER);
	eff.add_shader("..\\resources\\shaders\\phong.frag", GL_FRAGMENT_SHADER);
	// Build effect
	eff.build();

	// Set camera properties
	cam.set_position(vec3(50.0f, 10.0f, 50.0f));
	cam.set_target(vec3(0.0f, 0.0f, 0.0f));
	auto aspect = static_cast<float>(renderer::get_screen_width()) / static_cast<float>(renderer::get_screen_height());
	cam.set_projection(quarter_pi<float>(), aspect, 2.414f, 1000.0f);
	return true;
}

bool update(float delta_time)
{
	if (glfwGetKey(renderer::get_window(), '1'))
		cam.set_position(vec3(50, 10, 250));
	if (glfwGetKey(renderer::get_window(), '2'))
		cam.set_position(vec3(-50, 10, 50));
	if (glfwGetKey(renderer::get_window(), '3'))
		cam.set_position(vec3(-50, 10, -50));
	if (glfwGetKey(renderer::get_window(), '4'))
		cam.set_position(vec3(50, 10, -50));
	if (glfwGetKey(renderer::get_window(), GLFW_KEY_5))
		glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
	if (glfwGetKey(renderer::get_window(), GLFW_KEY_6))
		glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);

	// Rotate the sphere
	//meshes["sphere"].get_transform().rotate(vec3(0.0f, half_pi<float>(), 0.0f) * delta_time);
	if (glfwGetKey(renderer::get_window(), GLFW_KEY_W))
	{
		
		meshes["plane"].get_transform().position.z += 0.5f;
		//root.update_children(meshes["plane"].get_transform().get_transform_matrix());
	}

	if (glfwGetKey(renderer::get_window(), GLFW_KEY_S))
	{

		meshes["plane"].get_transform().position.z -= 0.5f;
		//root.update_children(meshes["plane"].get_transform().get_transform_matrix());
	}


	if (glfwGetKey(renderer::get_window(), GLFW_KEY_O))
	{

		meshes["plane"].get_transform().scale += -0.02;
	}

	if (glfwGetKey(renderer::get_window(), GLFW_KEY_P))
	{

		meshes["plane"].get_transform().scale += 0.02;
	}

	if (glfwGetKey(renderer::get_window(), GLFW_KEY_LEFT))
	{
		rho += pi<float>() * delta_time;
		meshes["plane"].get_transform().orientation = rotate(quat(), rho, yrot);
	}

	if (glfwGetKey(renderer::get_window(), GLFW_KEY_RIGHT))
	{
		rho -= pi<float>() * delta_time;
		meshes["plane"].get_transform().orientation = rotate(quat(), rho, yrot);
	}


	root.update_children(meshes["plane"].get_transform().get_transform_matrix());
	child2.update_local(meshes["plane"].get_transform().get_transform_matrix());

	cam.update(delta_time);

	return true;
}

bool render()
{
	

	cout << "x: " << meshes["plane"].get_transform().position.x << ", y: " << meshes["plane"].get_transform().position.y << ", z: " << meshes["plane"].get_transform().position.z << endl;

	// Bind effect
	renderer::bind(eff);
	// Create MVP matrix
	auto M = meshes["plane"].get_transform().get_transform_matrix();
	auto V = cam.get_view();
	auto P = cam.get_projection();
	auto MVP = P * V * M;
	//// Set MVP matrix uniform
	//glUniformMatrix4fv(
	//	eff.get_uniform_location("MVP"), // Location of uniform
	//	1, // Number of values - 1 mat4
	//	GL_FALSE, // Transpose the matrix?
	//	value_ptr(MVP)); // Pointer to matrix data
	//
	//// ********************
	//// Set M matrix uniform
	//// ********************
	//glUniformMatrix4fv(
	//	eff.get_uniform_location("M"), // Location of uniform
	//	1, // Number of values - 1 mat4
	//	GL_FALSE, // Transpose the matrix?
	//	value_ptr(M)); // Pointer to matrix data
	//
	//// ***********************
	//// Set N matrix uniform
	//// - remember - 3x3 matrix
	//// ***********************
	//auto N = meshes["plane"].get_transform().get_normal_matrix();
	//
	//glUniformMatrix3fv(
	//	eff.get_uniform_location("N"), // Location of uniform
	//	1, // Number of values - 1 mat4
	//	GL_FALSE, // Transpose the matrix?
	//	value_ptr(N)); // Pointer to normal matrix data
	//
	//// *************
	//// Bind material
	//// *************
	//renderer::bind(meshes["plane"].get_material(), "mat");
	//
	//// **********
	//// Bind light
	//// **********
	//renderer::bind(light, "light");
	//
	//// ************
	//// Bind texture
	//// ************
	//renderer::bind(tex, 0);
	//
	//// ***************
	//// Set tex uniform
	//// ***************
	//glUniform1i(eff.get_uniform_location("tex"), 0);
	//
	//// *****************************
	//// Set eye position
	//// - Get this from active camera
	//// *****************************
	//glUniform3f(eff.get_uniform_location("eye_pos"), cam.get_position().x, cam.get_position().y, cam.get_position().z);
	//
	//// Render mesh
	//renderer::render(meshes["plane"]);

	root.render_children(V, P, cam.get_position(), eff, light);



	//// Create MVP matrix
	//M = child1.m_worldTransform; // meshes["box"].get_transform().get_transform_matrix(); //
	//V = cam.get_view();
	//P = cam.get_projection();
	//MVP = P * V * M;
	//// Set MVP matrix uniform
	//glUniformMatrix4fv(
	//	eff.get_uniform_location("MVP"), // Location of uniform
	//	1, // Number of values - 1 mat4
	//	GL_FALSE, // Transpose the matrix?
	//	value_ptr(MVP)); // Pointer to matrix data
	//
	//// ********************
	//// Set M matrix uniform
	//// ********************
	//glUniformMatrix4fv(
	//	eff.get_uniform_location("M"), // Location of uniform
	//	1, // Number of values - 1 mat4
	//	GL_FALSE, // Transpose the matrix?
	//	value_ptr(M)); // Pointer to matrix data
	//
	//// ***********************
	//// Set N matrix uniform
	//// - remember - 3x3 matrix
	//// ***********************
	//N = meshes["box"].get_transform().get_normal_matrix();
	//
	//glUniformMatrix3fv(
	//	eff.get_uniform_location("N"), // Location of uniform
	//	1, // Number of values - 1 mat4
	//	GL_FALSE, // Transpose the matrix?
	//	value_ptr(N)); // Pointer to normal matrix data
	//
	//// *************
	//// Bind material
	//// *************
	//renderer::bind(meshes["box"].get_material(), "mat");
	//
	//// **********
	//// Bind light
	//// **********
	//renderer::bind(light, "light");
	//
	//// ************
	//// Bind texture
	//// ************
	//renderer::bind(tex, 0);
	//
	//// ***************
	//// Set tex uniform
	//// ***************
	//glUniform1i(eff.get_uniform_location("tex"), 0);
	//
	//// *****************************
	//// Set eye position
	//// - Get this from active camera
	//// *****************************
	//glUniform3f(eff.get_uniform_location("eye_pos"), cam.get_position().x, cam.get_position().y, cam.get_position().z);
	//
	//// Render mesh
	//renderer::render(meshes["box"]);
	//
	//
	//// Create MVP matrix
	//M = child2.m_worldTransform; // meshes["box"].get_transform().get_transform_matrix(); //
	//V = cam.get_view();
	//P = cam.get_projection();
	//MVP = P * V * M;
	//// Set MVP matrix uniform
	//glUniformMatrix4fv(
	//	eff.get_uniform_location("MVP"), // Location of uniform
	//	1, // Number of values - 1 mat4
	//	GL_FALSE, // Transpose the matrix?
	//	value_ptr(MVP)); // Pointer to matrix data
	//
	//// ********************
	//// Set M matrix uniform
	//// ********************
	//glUniformMatrix4fv(
	//	eff.get_uniform_location("M"), // Location of uniform
	//	1, // Number of values - 1 mat4
	//	GL_FALSE, // Transpose the matrix?
	//	value_ptr(M)); // Pointer to matrix data
	//
	//// ***********************
	//// Set N matrix uniform
	//// - remember - 3x3 matrix
	//// ***********************
	//N = meshes["tetra"].get_transform().get_normal_matrix();
	//
	//glUniformMatrix3fv(
	//	eff.get_uniform_location("N"), // Location of uniform
	//	1, // Number of values - 1 mat4
	//	GL_FALSE, // Transpose the matrix?
	//	value_ptr(N)); // Pointer to normal matrix data
	//
	//// *************
	//// Bind material
	//// *************
	//renderer::bind(meshes["tetra"].get_material(), "mat");
	//
	//// **********
	//// Bind light
	//// **********
	//renderer::bind(light, "light");
	//
	//// ************
	//// Bind texture
	//// ************
	//renderer::bind(tex, 0);
	//
	//// ***************
	//// Set tex uniform
	//// ***************
	//glUniform1i(eff.get_uniform_location("tex"), 0);
	//
	//// *****************************
	//// Set eye position
	//// - Get this from active camera
	//// *****************************
	//glUniform3f(eff.get_uniform_location("eye_pos"), cam.get_position().x, cam.get_position().y, cam.get_position().z);
	//
	//// Render mesh
	//renderer::render(meshes["tetra"]);


	//// Render meshes
	//for (auto &e : meshes)
	//{
	//	auto m = e.second;
	//	// Bind effect
	//	renderer::bind(eff);
	//	// Create MVP matrix
	//	auto M = m.get_transform().get_transform_matrix();
	//	auto V = cam.get_view();
	//	auto P = cam.get_projection();
	//	auto MVP = P * V * M;
	//	// Set MVP matrix uniform
	//	glUniformMatrix4fv(
	//		eff.get_uniform_location("MVP"), // Location of uniform
	//		1, // Number of values - 1 mat4
	//		GL_FALSE, // Transpose the matrix?
	//		value_ptr(MVP)); // Pointer to matrix data
	//
	//	// ********************
	//	// Set M matrix uniform
	//	// ********************
	//	glUniformMatrix4fv(
	//		eff.get_uniform_location("M"), // Location of uniform
	//		1, // Number of values - 1 mat4
	//		GL_FALSE, // Transpose the matrix?
	//		value_ptr(M)); // Pointer to matrix data
	//
	//	// ***********************
	//	// Set N matrix uniform
	//	// - remember - 3x3 matrix
	//	// ***********************
	//	auto N = m.get_transform().get_normal_matrix();
	//
	//	glUniformMatrix3fv(
	//		eff.get_uniform_location("N"), // Location of uniform
	//		1, // Number of values - 1 mat4
	//		GL_FALSE, // Transpose the matrix?
	//		value_ptr(N)); // Pointer to normal matrix data
	//
	//	// *************
	//	// Bind material
	//	// *************
	//	renderer::bind(m.get_material(), "mat");
	//
	//	// **********
	//	// Bind light
	//	// **********
	//	renderer::bind(light, "light");
	//
	//	// ************
	//	// Bind texture
	//	// ************
	//	renderer::bind(tex, 0);
	//
	//	// ***************
	//	// Set tex uniform
	//	// ***************
	//	glUniform1i(eff.get_uniform_location("tex"), 0);
	//
	//	// *****************************
	//	// Set eye position
	//	// - Get this from active camera
	//	// *****************************
	//	glUniform3f(eff.get_uniform_location("eye_pos"), cam.get_position().x, cam.get_position().y, cam.get_position().z);
	//
	//	// Render mesh
	//	renderer::render(m);
	//}

	return true;
}

void main()
{
	// Create application
	app application;
	// Set load content, update and render methods
	application.set_load_content(load_content);
	application.set_update(update);
	application.set_render(render);
	// Run application
	application.run();
}