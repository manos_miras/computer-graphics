#include <graphics_framework.h>
#include <glm\glm.hpp>

using namespace std;
using namespace graphics_framework;
using namespace glm;

map<string, mesh> meshes;
effect main_eff;
effect shadow_eff;
texture tex;
target_camera cam;
spot_light spot;
directional_light light;
shadow_map shadow;
vec3 tempPosition = vec3(30.0f, 30.0f, 0.0f);

bool load_content()
{

    // *****************
    // Create shadow map
    // - use screen size
    // *****************
	shadow = shadow_map(renderer::get_screen_width(), renderer::get_screen_height());
    // Create plane mesh
    meshes["plane"] = mesh(geometry_builder::create_plane());

    // *********************************************
    // Create "teapot" mesh by loading in teapot.3ds
    // *********************************************
	meshes["teapot"] = geometry("..\\resources\\models\\teapot.obj");
	
    // ***********************************************
    // Need to rotate the teapot on y by negative pi/2
    // ***********************************************
	meshes["teapot"].get_transform().rotate(quat(-half_pi<float>(), 0.0f, 1.0f, 0.0f));
    // *****************
    // Scale the teapot
    // - (0.1, 0.1, 0.1)
    // *****************
	meshes["teapot"].get_transform().scale = vec3(0.1f, 0.1f, 0.1f);
    // ************
    // Load texture
    // ************
	tex = texture("../resources/textures/checked.gif");
    // ***********************
    // Set materials
    // - all emissive is black
    // - all specular is white
    // - all shininess is 25
    // ***********************
	material mat;
	mat.set_emissive(vec4(0.0f, 0.0f, 0.0f, 0.0f));
	mat.set_diffuse(vec4(1.0f, 1.0f, 1.0f, 1.0f));
	mat.set_specular(vec4(1.0f, 1.0f, 1.0f, 1.0f));
	mat.set_shininess(25.0f);

	// White plane
	meshes["plane"].set_material(mat);
	//meshes["plane"].get_material().set_diffuse(vec4(1.0f, 1.0f, 1.0f, 1.0f));

	// White teapot
	meshes["teapot"].set_material(mat);
	//meshes["teapot"].get_material().set_diffuse(vec4(1.0f, 1.0f, 1.0f, 1.0f));

    // *******************
    // Set spot properties
    // *******************
	// Pos (20, 30, 0)
	spot.set_position(vec3(-30.0f, -30.0f, 0.0f));
	// White
	spot.set_light_colour(vec4(1.0f, 1.0f, 1.0f, 1.0f));
	// Direction (-1, -1, 0) normalized
	spot.set_direction(normalize(vec3(-1.0f, -1.0f, 0.0f)));
	// 50 range
	spot.set_range(100.0f);
	// 10 power
	spot.set_power(10.0f);

	light.set_direction(normalize(vec3(-1.0f, 1.0f, 0.0f)));
	light.set_light_colour(vec4(1.0f, 1.0f, 1.0f, 1.0f));
	light.set_ambient_intensity(vec4(0.3f, 0.3f, 0.3f, 0.3f));

    // Load in shaders
    main_eff.add_shader("..\\resources\\shaders\\shadow.vert", GL_VERTEX_SHADER);
    vector<string> frag_shaders
    {
        "shader.frag",
        //"..\\resources\\shaders\\parts\\spot.frag",
		"..\\resources\\shaders\\parts\\direction.frag",
        "..\\resources\\shaders\\parts\\shadow.frag"
    };
    main_eff.add_shader(frag_shaders, GL_FRAGMENT_SHADER);

    //shadow_eff.add_shader("..\\resources\\shaders\\spot.vert", GL_VERTEX_SHADER);
    //shadow_eff.add_shader("..\\resources\\shaders\\spot.frag", GL_FRAGMENT_SHADER);

	shadow_eff.add_shader(".\\multi.vert", GL_VERTEX_SHADER);
	shadow_eff.add_shader(".\\multi.frag", GL_FRAGMENT_SHADER);
	shadow_eff.add_shader("..\\resources\\shaders\\parts\\direction.frag", GL_FRAGMENT_SHADER);

    // Build effects
    main_eff.build();
    shadow_eff.build();

    // Set camera properties
    cam.set_position(vec3(0.0f, 50.0f, 75.0f));
    cam.set_target(vec3(0.0f, 0.0f, 0.0f));
    auto aspect = static_cast<float>(renderer::get_screen_width()) / static_cast<float>(renderer::get_screen_height());
    cam.set_projection(quarter_pi<float>(), aspect, 2.414f, 10000.0f);
    return true;
}

bool update(float delta_time)
{
    // Rotate the teapot
	meshes["teapot"].get_transform().rotate(vec3(0.0f, half_pi<float>() * delta_time, 0.0f));

    // ****************************************************
    // Update the shadow map properties from the spot light
    // ****************************************************
	
	shadow.light_position = tempPosition; //spot.get_position();
	shadow.light_dir = light.get_direction(); //spot.get_direction();
	

    // Press s to save
    if (glfwGetKey(renderer::get_window(), 'S') == GLFW_PRESS)
        shadow.buffer->save("test.png");

	// Spot light rotation
	if (glfwGetKey(renderer::get_window(), GLFW_KEY_O))
	{
		//spot.rotate(vec3(0.0f, half_pi<float>() * delta_time, 0.0f));
		tempPosition.z--;
	}

	if (glfwGetKey(renderer::get_window(), GLFW_KEY_P))
	{
		//spot.rotate(vec3(0.0f, -half_pi<float>() * delta_time, 0.0f));
		tempPosition.z++;
	}

	// Spot light movement
	if (glfwGetKey(renderer::get_window(), GLFW_KEY_LEFT))
	{
		//spot.move(vec3(1.0f, 0.0f, 0.0f));
		//tempPosition.x--;
		meshes["teapot"].get_transform().translate(vec3(-1.0f, 0.0f, 0.0f));
	}

	if (glfwGetKey(renderer::get_window(), GLFW_KEY_RIGHT))
	{
		//spot.move(vec3(-1.0f, 0.0f, 0.0f));
		//tempPosition.x++;
		meshes["teapot"].get_transform().translate(vec3(1.0f, 0.0f, 0.0f));
	}

	if (glfwGetKey(renderer::get_window(), GLFW_KEY_DOWN))
	{
		//spot.move(vec3(0.0f, 0.0f, 1.0f));
		meshes["teapot"].get_transform().translate(vec3(0.0f, 0.0f, 1.0f));
	}

	if (glfwGetKey(renderer::get_window(), GLFW_KEY_UP))
	{
		//spot.move(vec3(0.0f, 0.0f, -1.0f));
		meshes["teapot"].get_transform().translate(vec3(0.0f, 0.0f, -1.0f));
	}




    cam.update(delta_time);

    return true;
}

bool render()
{
    // *******************************
    // Set render target to shadow map
    // *******************************
	renderer::set_render_target(shadow);

    // **********************
    // Clear depth buffer bit
    // **********************
	glClear(GL_DEPTH_BUFFER_BIT);

    // ****************************
    // Set render mode to cull face
    // ****************************
	glCullFace(GL_FRONT); // or GL_BACK??

    // Bind shader
    renderer::bind(shadow_eff);

    // Render meshes
    for (auto &e : meshes)
    {
        auto m = e.second;
        // Create MVP matrix
        auto M = m.get_transform().get_transform_matrix();
        // *********************************
        // View matrix taken from shadow map
        // *********************************
		glm::vec3 lightInvDir = glm::vec3(0.5f, 2, 2);
		auto V = glm::lookAt(lightInvDir, glm::vec3(0, 0, 0) + light.get_direction(), glm::vec3(0, 1, 0)); //shadow.get_view();
		shadow.get_view();
        auto P = cam.get_projection();
        auto MVP = P * V * M;
        // Set MVP matrix uniform
        glUniformMatrix4fv(
            shadow_eff.get_uniform_location("MVP"), // Location of uniform
            1, // Number of values - 1 mat4
            GL_FALSE, // Transpose the matrix?
            value_ptr(MVP)); // Pointer to matrix data
        // Render mesh
        renderer::render(m);
    }

    // ************************************
    // Set render target back to the screen
    // ************************************
	renderer::set_render_target();

    // *********************
    // Set cull face to back
    // *********************
	glCullFace(GL_BACK);
    // Bind shader
    renderer::bind(main_eff);

    // Render meshes
    for (auto &e : meshes)
    {
        auto m = e.second;
        // Create MVP matrix
        auto M = m.get_transform().get_transform_matrix();
        auto V = cam.get_view();
        auto P = cam.get_projection();
        auto MVP = P * V * M;
        // Set MVP matrix uniform
        glUniformMatrix4fv(
            main_eff.get_uniform_location("MVP"), // Location of uniform
            1, // Number of values - 1 mat4
            GL_FALSE, // Transpose the matrix?
            value_ptr(MVP)); // Pointer to matrix data
        // Set M matrix uniform
        glUniformMatrix4fv(
            main_eff.get_uniform_location("M"),
            1,
            GL_FALSE,
            value_ptr(M));
        // Set N matrix uniform
        glUniformMatrix3fv(
            main_eff.get_uniform_location("N"),
            1,
            GL_FALSE,
            value_ptr(m.get_transform().get_normal_matrix()));

        // *******************
        // Set light transform
        // *******************

		glm::vec3 lightInvDir = glm::vec3(0.5f, 2, 2);

		// Compute the MVP matrix from the light's point of view
		glm::mat4 depthProjectionMatrix = glm::ortho<float>(-10, 10, -10, 10, -10, 20);
		glm::mat4 depthViewMatrix = glm::lookAt(lightInvDir, glm::vec3(0, 0, 0) + light.get_direction(), glm::vec3(0, 1, 0));
		glm::mat4 depthModelMatrix = glm::mat4(1.0);
		glm::mat4 depthMVP = depthProjectionMatrix * depthViewMatrix * depthModelMatrix;

		glUniformMatrix4fv(
			main_eff.get_uniform_location("lightMVP"),
			1,
			GL_FALSE,
			value_ptr(depthMVP)); // value_ptr(P * shadow.get_view() * M));
        // Bind material
        renderer::bind(m.get_material(), "mat");
        // Bind spot lights
        //renderer::bind(spot, "spot");
		// Bind directional light
		renderer::bind(light, "light");
        // Bind texture
        renderer::bind(tex, 0);
        // Set tex uniform
        glUniform1i(main_eff.get_uniform_location("tex"), 0);
        // Set eye position
        glUniform3fv(main_eff.get_uniform_location("eye_pos"), 1, value_ptr(cam.get_position()));
        // ***********************
        // Bind shadow map texture
        // - use texture unit 1
        // ***********************
		// Bind shadow texture
		renderer::bind(shadow.buffer->get_depth(), 1);
		// Set shadow texture uniform
		glUniform1i(main_eff.get_uniform_location("shadow_map"), 1);

        // Render mesh
        renderer::render(m);
    }

    return true;
}

void main()
{
    // Create application
    app application;
    // Set load content, update and render methods
    application.set_load_content(load_content);
    application.set_update(update);
    application.set_render(render);
    // Run application
    application.run();
}