#include <graphics_framework.h>
#include <glm\glm.hpp>

using namespace std;
using namespace graphics_framework;
using namespace glm;

effect eff;
texture tex;
mesh m;
target_camera cam;

double xpos, ypos, xposNext, yposNext;

float theta = 20.0f;
float rho = 0.0f;

vec3 xrot(1.0f, 0.0f, 0.0f);
vec3 yrot(0.0f, 1.0f, 0.0f);

quat rx, ry;

bool load_content()
{
	// *************
	// Load in model
	// *************
	m = (geometry("..\\resources\\models\\dummy_obj.obj"));

	// ***************
	// Load in texture
	// ***************
	tex = texture("..\\resources\\textures\\dummy_wood.jpg");

	// Change position
	m.get_transform().position = vec3(0.0f,-50.0f,5.0f);
	// Change Scale
	m.get_transform().scale = vec3(0.7f, 0.7f, 0.7f);
	// Load in shaders
	eff.add_shader("..\\resources\\shaders\\simple_texture.vert", GL_VERTEX_SHADER);
	eff.add_shader("..\\resources\\shaders\\simple_texture.frag", GL_FRAGMENT_SHADER);
	// Build effect
	eff.build();

	// Set camera properties
	cam.set_position(vec3(200.0f, 200.0f, 200.0f));
	cam.set_target(vec3(0.0f, 0.0f, 0.0f));
	auto aspect = static_cast<float>(renderer::get_screen_width()) / static_cast<float>(renderer::get_screen_height());
	cam.set_projection(quarter_pi<float>(), aspect, 2.414f, 1000.0f);
	return true;
}

bool update(float delta_time)
{

	// Cursor - rotation

	int state = glfwGetMouseButton(renderer::get_window(), GLFW_MOUSE_BUTTON_LEFT);

	if (state == GLFW_PRESS)
	{
		// Makes cursor invisible
		glfwSetInputMode(renderer::get_window(), GLFW_CURSOR, GLFW_CURSOR_DISABLED);

		// Store x and y positions before updating them 
		// to see in which direction the object has to rotate.
		xpos = xposNext;
		ypos = yposNext;

		// Gets cursor position
		glfwGetCursorPos(renderer::get_window(), &xposNext, &yposNext);

		// Makes centre of screen origin points x,y(0.0,0.0)
		xposNext -= renderer::get_screen_width() / 2.0f;
		yposNext -= renderer::get_screen_height() / 2.0f;

		// Rotate right
		if (yposNext > ypos)
			rho -= pi<float>() * 2 * delta_time;
		// Rotate left
		if (yposNext < ypos)
			rho += pi<float>() * 2 * delta_time;
		// Rotate down
		if (xposNext > xpos)
			theta += pi<float>() * 2 * delta_time;
		// Rotate up
		if (xposNext < xpos)
			theta -= pi<float>() * 2 * delta_time;

		cout << "x = " << xpos << endl;
		cout << "y = " << ypos << endl;



		if (glfwGetKey(renderer::get_window(), GLFW_KEY_UP))
			theta -= pi<float>() * delta_time;
		if (glfwGetKey(renderer::get_window(), GLFW_KEY_DOWN))
			theta += pi<float>() * delta_time;
		if (glfwGetKey(renderer::get_window(), GLFW_KEY_RIGHT))
			rho -= pi<float>() * delta_time;
		if (glfwGetKey(renderer::get_window(), GLFW_KEY_LEFT))
			rho += pi<float>() * delta_time;
	}
	else
	{
		// Makes cursor visible again
		glfwSetInputMode(renderer::get_window(), GLFW_CURSOR, GLFW_CURSOR_NORMAL);
	}

	// Update the camera
	cam.update(delta_time);
	return true;
}

bool render()
{
	// Bind effect
	renderer::bind(eff);
	// Create MVP matrix
	auto M = m.get_transform().get_transform_matrix();

	rx = rotate(m.get_transform().orientation, rho, xrot);
	ry = rotate(m.get_transform().orientation, theta, yrot);

	mat4 R = mat4_cast(rx * ry); // *ry;

	M = M * R;

	auto V = cam.get_view();
	auto P = cam.get_projection();
	auto MVP = P * V * M;
	// Set MVP matrix uniform
	glUniformMatrix4fv(
		eff.get_uniform_location("MVP"),
		1,
		GL_FALSE,
		value_ptr(MVP));

	// Bind and set texture
	renderer::bind(tex, 0);
	glUniform1i(eff.get_uniform_location("tex"), 0);

	// Render mesh
	renderer::render(m);

	return true;
}

void main()
{
	// Create application
	app application;
	// Set load content, update and render methods
	application.set_load_content(load_content);
	application.set_update(update);
	application.set_render(render);
	// Run application
	application.run();
}