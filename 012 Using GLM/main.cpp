#include <iostream>
#include <glm\glm.hpp>
#include <glm\gtc\quaternion.hpp>
#include <glm\gtc\constants.hpp>
#include <glm\gtc\matrix_transform.hpp>
#include <glm\gtx\euler_angles.hpp>
#include <glm\gtx\projection.hpp>

using namespace std;
using namespace glm;

int main()
{
	float x, w = 1.0f;
	float y, z = 0.0f;

	// 2D vectors
	vec2 a(x, y);
	vec2 b(-x, y);

	// 3D vectors
	vec3 c(x, y, z);
	vec3 d(-x, y, z);

	// 4D vectors
	vec4 e(x, y, z, w);
	vec4 f(-x, y, z, w);

	// Converting 2D vector to 3D vector
	vec3 g(vec2(x, y), z);

	// Converting 2D vector to 4D vector
	vec4 h(vec2(x, y), z, w);

	// Converting 3D vector to 2D vector
	vec2 i(vec3(x, y, z)); // z component is dropped

	// Converting 3D vector to 4D vector
	vec4 j(vec3(x, y, z), w);

	// Converting 4D vector to 2D vector
	vec2 k(vec4(x, y, z, w)); // z and w components are dropped

	// Converting 4D vector to 3D vector
	vec3 l(vec4(x, y, z, w)); // w component is dropped

	// Vector Operations

	// Addition, Subtraction

	vec2 add2 = a + b;
	vec2 sub2 = a - b;

	vec3 add3 = c + d;
	vec3 sub3 = c - d;

	vec4 add4 = e + f;
	vec4 sub4 = e - f;

	// Scaling

	vec2 scmu2 = a * 2.0f;
	vec2 scdiv2 = b / 2.0f;

	vec3 scmu3 = c * 6.0f;
	vec3 scdiv3 = d / 2.0f;

	vec4 scmu4 = e * 3.0f;
	vec4 scdiv4 = f / 4.0f;

	// Length

	float l2 = length(a);

	float l3 = length(c);

	float l4 = length(e);

	// Normalizing

	vec2 n2 = normalize(b);

	vec3 n3 = normalize(d);
	
	vec4 n4 = normalize(f);

	// Dot product

	float d2 = dot(a, b);

	float d3 = dot(c, d);

	float d4 = dot(e, f);

	//Vector Projection

	vec2 p2 = proj(a, b);

	vec3 p3 = proj(c, d);

	vec4 p4 = proj(e, f);

	// Cross product

	vec3 c3 = cross(c, d);

	// Matrices

	mat4 m(1.0f); //  Identity matrix

	mat4 n(1.0f);

	mat4x2 o(1.0f);

	// Matrix Operations

	mat4 add = m + n;
	mat4 sub = m - n;

	mat4 scmu = m * 10.0f;
	mat4 scdiv = n / 2.0f;

	mat4 mul1 = m * n;

	mat4x2 mul2 = o * m;

	vec4 v = m * vec4(c, 1.0f);

	mat4 T = translate(mat4(1.0f), vec3(-3.0f, 0.0f, 0.0f));

	vec3 translation = vec3(T * vec4(c, 1.0f));
}