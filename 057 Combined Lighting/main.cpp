#include <graphics_framework.h>
#include <glm\glm.hpp>

using namespace std;
using namespace graphics_framework;
using namespace glm;

map<string, mesh> meshes;
effect eff;
texture tex;
free_camera cam;

// Initial mouse input
double cursor_x = 0.0;
double cursor_y = 0.0;

bool initialise()
{
	// ********************************
	// Set input mode - hide the cursor
	// ********************************
	glfwSetInputMode(renderer::get_window(), GLFW_CURSOR, GLFW_CURSOR_DISABLED);

	// ******************************
	// Capture initial mouse position
	// ******************************
	glfwGetCursorPos(renderer::get_window(), &cursor_x, &cursor_y);

	return true;
}


bool load_content()
{
	// Create plane mesh
	meshes["plane"] = mesh(geometry_builder::create_plane());

	// Create scene
	meshes["box"] = mesh(geometry_builder::create_box());
	meshes["tetra"] = mesh(geometry_builder::create_tetrahedron());
	meshes["pyramid"] = mesh(geometry_builder::create_pyramid());
	meshes["disk"] = mesh(geometry_builder::create_disk(20));
	meshes["cylinder"] = mesh(geometry_builder::create_cylinder(20, 20));
	meshes["sphere"] = mesh(geometry_builder::create_sphere(20, 20));
	meshes["torus"] = mesh(geometry_builder::create_torus(20, 20, 1.0f, 5.0f));

	// Transform objects
	meshes["box"].get_transform().scale = vec3(5.0f, 5.0f, 5.0f);
	meshes["box"].get_transform().translate(vec3(-10.0f, 2.5f, -30.0f));
	meshes["tetra"].get_transform().scale = vec3(4.0f, 4.0f, 4.0f);
	meshes["tetra"].get_transform().translate(vec3(-30.0f, 10.0f, -10.0f));
	meshes["pyramid"].get_transform().scale = vec3(5.0f, 5.0f, 5.0f);
	meshes["pyramid"].get_transform().translate(vec3(-10.0f, 7.5f, -30.0f));
	meshes["disk"].get_transform().scale = vec3(3.0f, 1.0f, 3.0f);
	meshes["disk"].get_transform().translate(vec3(-10.0f, 11.5f, -30.0f));
	meshes["disk"].get_transform().rotate(vec3(half_pi<float>(), 0.0f, 0.0f));
	meshes["cylinder"].get_transform().scale = vec3(5.0f, 5.0f, 5.0f);
	meshes["cylinder"].get_transform().translate(vec3(-25.0f, 2.5f, -25.0f));
	meshes["sphere"].get_transform().scale = vec3(2.5f, 2.5f, 2.5f);
	meshes["sphere"].get_transform().translate(vec3(-25.0f, 10.0f, -25.0f));
	meshes["torus"].get_transform().translate(vec3(-25.0f, 10.0f, -25.0f));
	meshes["torus"].get_transform().rotate(vec3(half_pi<float>(), 0.0f, 0.0f));

	// Load in shaders
	eff.add_shader("..\\resources\\shaders\\combined_lighting.vert", GL_VERTEX_SHADER);
	eff.add_shader("..\\resources\\shaders\\combined_lighting.frag", GL_FRAGMENT_SHADER);
	// Build effect
	eff.build();

	// Set camera properties
	cam.set_position(vec3(50.0f, 10.0f, 50.0f));
	cam.set_target(vec3(0.0f, 0.0f, 0.0f));
	auto aspect = static_cast<float>(renderer::get_screen_width()) / static_cast<float>(renderer::get_screen_height());
	cam.set_projection(quarter_pi<float>(), aspect, 2.414f, 1000.0f);
	return true;
}

bool update(float delta_time)
{
	if (glfwGetKey(renderer::get_window(), '1'))
		cam.set_position(vec3(50, 10, 50));
	if (glfwGetKey(renderer::get_window(), '2'))
		cam.set_position(vec3(-50, 10, 50));
	if (glfwGetKey(renderer::get_window(), '3'))
		cam.set_position(vec3(-50, 10, -50));
	if (glfwGetKey(renderer::get_window(), '4'))
		cam.set_position(vec3(50, 10, -50));

	// Rotate the sphere
	meshes["sphere"].get_transform().rotate(vec3(0.0f, half_pi<float>(), 0.0f) * delta_time);

	// **** MOVE CAMERA ****

	// The ratio of pixels to rotation - remember the fov
	static double ratio_width = quarter_pi<float>() / static_cast<float>(renderer::get_screen_width());
	static double ratio_height = (quarter_pi<float>() * (static_cast<float>(renderer::get_screen_height()) / static_cast<float>(renderer::get_screen_width()))) / static_cast<float>(renderer::get_screen_height());

	double current_x;
	double current_y;
	// *******************************
	// Get the current cursor position
	// *******************************
	glfwGetCursorPos(renderer::get_window(), &current_x, &current_y);


	// ***************************************************
	// Calculate delta of cursor positions from last frame
	// ***************************************************
	double delta_x = current_x - cursor_x;
	double delta_y = current_y - cursor_y;

	// *************************************************************
	// Multiply deltas by ratios - gets actual change in orientation
	// *************************************************************
	delta_x = delta_x * ratio_width;
	delta_y = delta_y * ratio_height;

	// *************************
	// Rotate cameras by delta
	// delta_y - x-axis rotation
	// delta_x - y-axis rotation
	// *************************
	cam.rotate(delta_x, -delta_y); // -delta_y, otherwise up and down is reversed
	// *******************************
	// Use keyboard to move the camera
	// - WSAD
	// *******************************

	const float speed = 1.0f; // Speed of camera movement

	vec3 translation(0.0f, 0.0f, 0.0f);

	// Check if key is pressed
	if (glfwGetKey(renderer::get_window(), GLFW_KEY_W))
		cam.move(vec3(0.0f, 0.0f, speed)); // translation = vec3(0.0f, 0.0f, 1.0f); (If you do it this way then you cant have 2 translations at the same time)
	if (glfwGetKey(renderer::get_window(), GLFW_KEY_S))
		cam.move(vec3(0.0f, 0.0f, -speed)); // translation = vec3(0.0f, 0.0f, -1.0f);
	if (glfwGetKey(renderer::get_window(), GLFW_KEY_A))
		cam.move(vec3(-speed, 0.0f, 0.0f)); // translation = vec3(-1.0f, 0.0f, 0.0f);
	if (glfwGetKey(renderer::get_window(), GLFW_KEY_D))
		cam.move(vec3(speed, 0.0f, 0.0f)); // translation = vec3(1.0f, 0.0f, 0.0f);
	// ***********
	// Move camera
	// ***********

	//cam.move(translation);

	// *****************
	// Update the camera
	// *****************
	cam.update(delta_time);


	// *****************
	// Update cursor pos
	// *****************
	cursor_x = current_x;
	cursor_y = current_y;
	
	cam.update(delta_time);

	return true;
}

bool render()
{
	// Render meshes
	for (auto &e : meshes)
	{
		auto m = e.second;
		// Bind effect
		renderer::bind(eff);
		// Create MVP matrix
		auto M = m.get_transform().get_transform_matrix();
		auto V = cam.get_view();
		auto P = cam.get_projection();
		auto MVP = P * V * M;
		// Set MVP matrix uniform
		glUniformMatrix4fv(
			eff.get_uniform_location("MVP"), // Location of uniform
			1, // Number of values - 1 mat4
			GL_FALSE, // Transpose the matrix?
			value_ptr(MVP)); // Pointer to matrix data

		// ********************
		// Set M matrix uniform
		// ********************
		glUniformMatrix4fv(
			eff.get_uniform_location("M"), // Location of uniform
			1, // Number of values - 1 mat4
			GL_FALSE, // Transpose the matrix?
			value_ptr(M)); // Pointer to matrix data


		// ***********************
		// Set N matrix uniform
		// - remember - 3x3 matrix
		// ***********************
		auto N = m.get_transform().get_normal_matrix(); // Normal matrix

		glUniformMatrix3fv(
			eff.get_uniform_location("N"), // Location of uniform
			1, // Number of values - 1 mat3
			GL_FALSE, // Transpose the matrix?
			value_ptr(N)); // Pointer to normal matrix data

		// **********************
		// Set ambient intensity
		// - (0.3, 0.3, 0.3, 1.0)
		// **********************
		glUniform4f(eff.get_uniform_location("ambient_intensity"), 0.3f, 0.3f, 0.3f, 1.0f);

		// **********************
		// Set light colour
		// - (1.0, 1.0, 1.0, 1.0)
		// **********************
		glUniform4f(eff.get_uniform_location("light_colour"), 1.0f, 1.0f, 1.0f, 1.0f);

		// *******************
		// Set light direction
		// - (1.0, 1.0, -1.0)
		// *******************
		glUniform3f(eff.get_uniform_location("light_dir"), 1.0f, 1.0f, -1.0f);

		// **********************
		// Set diffuse reflection
		// - all objects red
		// **********************
		glUniform4f(eff.get_uniform_location("diffuse_reflection"), 0.2f, 0.2f, 0.9f, 1.0f);

		// ***********************
		// Set specular reflection
		// - white
		// ***********************
		glUniform4f(eff.get_uniform_location("specular_reflection"), 1.0f, 1.0f, 1.0f, 1.0f);

		// *************
		// Set shininess
		// - Use 50.0f
		// *************
		glUniform1f(eff.get_uniform_location("shininess"), 50.0f);

		// *****************************
		// Set eye position
		// - Get this from active camera
		// *****************************
		glUniform3f(eff.get_uniform_location("eye_pos"), cam.get_position().x, cam.get_position().y, cam.get_position().z);
		
		// Render mesh
		renderer::render(m);
	}

	return true;
}

void main()
{
	// Create application
	app application;
	// Set load content, update and render methods
	application.set_load_content(load_content);
	application.set_initialise(initialise);
	application.set_update(update);
	application.set_render(render);
	// Run application
	application.run();
}